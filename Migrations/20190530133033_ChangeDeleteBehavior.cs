﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace rovashop.Migrations
{
    public partial class ChangeDeleteBehavior : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_product_supplier_SupplierId",
                table: "product");

            migrationBuilder.DropForeignKey(
                name: "FK_rating_product_ProductId",
                table: "rating");

            migrationBuilder.AddForeignKey(
                name: "FK_product_supplier_SupplierId",
                table: "product",
                column: "SupplierId",
                principalTable: "supplier",
                principalColumn: "SupplierId",
                onDelete: ReferentialAction.SetNull);

            migrationBuilder.AddForeignKey(
                name: "FK_rating_product_ProductId",
                table: "rating",
                column: "ProductId",
                principalTable: "product",
                principalColumn: "ProductId",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_product_supplier_SupplierId",
                table: "product");

            migrationBuilder.DropForeignKey(
                name: "FK_rating_product_ProductId",
                table: "rating");

            migrationBuilder.AddForeignKey(
                name: "FK_product_supplier_SupplierId",
                table: "product",
                column: "SupplierId",
                principalTable: "supplier",
                principalColumn: "SupplierId",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_rating_product_ProductId",
                table: "rating",
                column: "ProductId",
                principalTable: "product",
                principalColumn: "ProductId",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
