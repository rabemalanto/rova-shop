using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using rovashop.Models;

namespace rovashop.Controllers
{
    public class AccountController : Controller
    {
      private UserManager<IdentityUser> userManager;
      private SignInManager<IdentityUser> signInManager;
      public AccountController(UserManager<IdentityUser> userMgr,    SignInManager<IdentityUser> signInMgr)
      {
        userManager = userMgr;
        signInManager = signInMgr;
      }
      [HttpGet]
      public IActionResult Login(string returnUrl)
      {
        ViewBag.returnUrl = returnUrl;
        return View();
      }
      [HttpPost("/api/account/login")]
      public async Task<IActionResult> Login([FromBody] LoginViewModel creds)
      {
        if (ModelState.IsValid && await DoLogin(creds))
        {
          return Ok();
        }
        return BadRequest();
      }
      [HttpPost("/api/account/logout")]
      public async Task<IActionResult> Logout()
      {
        await signInManager.SignOutAsync();
        return Ok();
      }
      private async Task<bool> DoLogin(LoginViewModel creds)
      {
        IdentityUser user = await userManager.FindByNameAsync(creds.Name);
        if (user != null)
        {
          await signInManager.SignOutAsync();
          Microsoft.AspNetCore.Identity.SignInResult result =
          await signInManager.PasswordSignInAsync(user, creds.Password, false, false);
          return result.Succeeded;
        }
        return false;
      }
    }
    

}
