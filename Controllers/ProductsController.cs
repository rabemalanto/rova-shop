using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using rovashop.Models;
using rovashop.Models.Concrete;
using rovashop.Models.Entity;

namespace rovashop.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Roles = "Administrator")]
    [ApiController]
    [ValidateAntiForgeryToken]
    public class ProductsController : ControllerBase
    {
        private readonly RovaShopDataContext _context;

        public ProductsController(RovaShopDataContext context)
        {
            _context = context;
        }

        [HttpGet]
        [AllowAnonymous]
        public IActionResult GetProducts(string category, string search, bool related, bool metadata = false) 
        {
          IQueryable<Product> query = _context.Products;
          if (!string.IsNullOrEmpty(category))
          {
            string catLower = category.ToLower();
            query = query.Where(_=>_.Category.ToLower().Contains(catLower));
          }
          if (!string.IsNullOrEmpty(search))
          {
            string searchLower = search.ToLower();
            query = query.Where(_=>_.Name.ToLower().Contains(searchLower) || _.Description.ToLower().Contains(searchLower));
          }
          if (related && HttpContext.User.IsInRole("Administrator"))
          {
            query = query.Include(p => p.Supplier).Include(p => p.Ratings);
            List<Product> data = query.ToList();
            data.ForEach(p => {
              if (p.Supplier != null)
              {
                p.Supplier.Products = null;
              }
              if (p.Ratings != null)
              {
                p.Ratings.ForEach(r => r.Product = null);
              }
            });
            return metadata ? CreateMetadata(data) : Ok(data);
          }
          else
          {
            return metadata ? CreateMetadata(query) : Ok(query);
          }
        }
        private IActionResult CreateMetadata(IEnumerable<Product> products)
        {
          return Ok(new
          {
            data = products,
            categories = _context.Products.Select(p => p.Category)
          .Distinct().OrderBy(c => c)
          });
        }
        // GET: api/Products/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetProduct([FromRoute] long id)
        {
          IQueryable<Product> query = _context.Products.Include(p => p.Ratings);
          if (HttpContext.User.IsInRole("Administrator"))
          {
            query = query.Include(p => p.Supplier)
            .ThenInclude(s => s.Products);
          }
          Product product = await query.FirstAsync(p => p.ProductId == id);
          //to break reference circular
          if (product != null)
            {
              if (product.Supplier != null) product.Supplier.Products = null;

              if (product.Ratings != null) {
                foreach (Rating r in product.Ratings)
                {
                  r.Product = null;
                }
              }
            }

            if (product == null)
            {
                return NotFound();
            }

            return Ok(product);
        }
        
        [HttpPut("{id}")]
        public async Task<IActionResult> ReplaceProduct(long id, [FromBody] ProductData pdata)
        {
          if (ModelState.IsValid)
          {
            Product p = pdata.Product;
            p.ProductId = id;
            if (p.Supplier != null && p.Supplier.SupplierId != 0)
            {
              _context.Attach(p.Supplier);
            }
            _context.Update(p);
            await _context.SaveChangesAsync();
            return Ok();
          }
          else
          {
            return BadRequest(ModelState);
          }
        }
        // POST: api/Products
        [HttpPost]
        public async Task<IActionResult> PostProduct([FromBody] ProductData productData)
        {
          if (ModelState.IsValid)
          {
            Product p = productData.Product;
            if (p.Supplier != null && p.Supplier.SupplierId != 0)
            {
              _context.Attach(p.Supplier);
            }
            _context.Add(p);
            await _context.SaveChangesAsync();
            return Ok(p.ProductId);
          }
          else
          {
            return BadRequest(ModelState);
          }
        }

        [HttpPatch("{id}")]
        public async Task<IActionResult> UpdateProduct(long id, [FromBody]JsonPatchDocument<ProductData> patch)
        {
          Product product = _context.Products
          .Include(p => p.Supplier)
          .First(p => p.ProductId == id);
          ProductData pdata = new ProductData { Product = product };
          patch.ApplyTo(pdata, ModelState);
          if (ModelState.IsValid && TryValidateModel(pdata))
          {
            if (product.Supplier != null && product.Supplier.SupplierId != 0)
            {
              _context.Attach(product.Supplier);
            }
            await _context.SaveChangesAsync();
            return Ok();
          }
          else
          {
            return BadRequest(ModelState);
          }
        }
        // DELETE: api/Products/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteProduct([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var product = await _context.Products.FindAsync(id);
            if (product == null)
            {
                return NotFound();
            }

            _context.Products.Remove(product);
            await _context.SaveChangesAsync();

            return Ok();
        }

        private bool ProductExists(long id)
        {
            return _context.Products.Any(e => e.ProductId == id);
        }
    }
}
