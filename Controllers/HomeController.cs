using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using rovashop.Models;
using rovashop.Models.Abstract;
using Microsoft.AspNetCore.Authorization;

namespace rovashop.Controllers
{
    public class HomeController : Controller
    {
      private IRepository repository;

      public HomeController(IRepository repo)
      {
        repository = repo;
        repository.InitSeeds();
      repository.InitSeedIdentity();
      }
        public IActionResult Index()
        {
          return View();
        }
      [Authorize]
      public string Protected()
      {
        return "You have been authenticated";
      }
  }
}
