using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using rovashop.Models.Concrete;
using rovashop.Models.Entity;

namespace rovashop.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Roles = "Administrator")]
    [ApiController]
  [ValidateAntiForgeryToken]
  public class OrderController : ControllerBase
    {
      private RovaShopDataContext _context;
      public OrderController(RovaShopDataContext context)
      {
      _context = context;
      }
    [HttpGet]
    [Route("api/order")]
    public IEnumerable<Order> GetOrders()
    {
      return _context.Orders
              .Include(o => o.Products).Include(o => o.Payment);
    }
    [HttpPost("{id}")]
    public void MarkShipped(long id)
    {
      Order order = _context.Orders.Find(id);
      if (order != null)
      {
        order.Shipped = true;
        _context.SaveChanges();
      }
    }
    [HttpPost]
    [AllowAnonymous]
    public async Task<IActionResult> CreateOrder([FromBody] Order order)
    {
      if (ModelState.IsValid)
      {
        order.OrderId = 0;
        order.Shipped = false;
        order.Payment.Total = GetPrice(order.Products);
        ProcessPayment(order.Payment);
        if (order.Payment.AuthCode != null)
        {
          _context.Add(order);
          await _context.SaveChangesAsync();
          return Ok(new
          {
            orderId = order.OrderId,
            authCode = order.Payment.AuthCode,
            amount = order.Payment.Total
          });
        }
        else
        {
          return BadRequest("Payment rejected");
        }
      }
      return BadRequest(ModelState);
    }
    private decimal GetPrice(IEnumerable<CartLine> lines)
    {
      IEnumerable<long> ids = lines.Select(l => l.ProductId);
      return _context.Products
      .Where(p => ids.Contains(p.ProductId))
      .Select(p => lines
      .First(l => l.ProductId == p.ProductId).Quantity * p.Price)
      .Sum();
    }
    private void ProcessPayment(Payment payment)
    {
      // integrate your payment system here
      payment.AuthCode = "12345";
    }
  }
}
