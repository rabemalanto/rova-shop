using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using rovashop.Models;
using rovashop.Models.Concrete;
using rovashop.Models.Entity;
using Microsoft.AspNetCore.Authorization;

namespace rovashop.Controllers
{
    [Route("api/[controller]")]
    [Authorize(Roles = "Administrator")]
    [ApiController]
    [ValidateAntiForgeryToken]
    public class SuppliersController : ControllerBase
    {
        private readonly RovaShopDataContext _context;

        public SuppliersController(RovaShopDataContext context)
        {
            _context = context;
        }

        // GET: api/Suppliers
        [HttpGet]
        [Route("api/suppliers")]
        public IEnumerable<Supplier> GetSuppliers()
        {
            return _context.Suppliers;
        }

        // GET: api/Suppliers/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetSupplier([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var supplier = await _context.Suppliers.FindAsync(id);

            if (supplier == null)
            {
                return NotFound();
            }

            return Ok(supplier);
        }

        
        [HttpPut("{id}")]
        public async Task<IActionResult> ReplaceSupplier(long id, [FromBody] SupplierData sdata)
        {
          if (ModelState.IsValid)
          {
            Supplier s = sdata.Supplier;
            s.SupplierId = id;
            _context.Update(s);
            await _context.SaveChangesAsync();
            return Ok();
          }
          else
          {
            return BadRequest(ModelState);
          }
        }
        // POST: api/Suppliers
        [HttpPost]
        public IActionResult CreateSupplier([FromBody]SupplierData sdata)
        {
          if (ModelState.IsValid)
          {
            Supplier s = sdata.Supplier;
            _context.Add(s);
            _context.SaveChanges();
            return Ok(s.SupplierId);
          }
          else
          {
            return BadRequest(ModelState);
          }
        }
        // DELETE: api/Suppliers/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteSupplier([FromRoute] long id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var supplier = await _context.Suppliers.FindAsync(id);
            if (supplier == null)
            {
                return NotFound();
            }

            _context.Suppliers.Remove(supplier);
            await _context.SaveChangesAsync();

            return Ok();
        }

        private bool SupplierExists(long id)
        {
            return _context.Suppliers.Any(e => e.SupplierId == id);
        }
    }
}
