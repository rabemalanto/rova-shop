using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using rovashop.Models;
using rovashop.Models.Entity;

namespace rovashop.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [ValidateAntiForgeryToken]
    public class SessionController : ControllerBase
    {
      [HttpGet("cart")]
      public IActionResult GetCart()
      {
        return Ok(HttpContext.Session.GetString("cart"));
      }
      [HttpPost("cart")]
      public void StoreCart([FromBody] ProductSelection[] products)
      {
        var jsonData = JsonConvert.SerializeObject(products);
        HttpContext.Session.SetString("cart", jsonData);
      }
    [HttpGet("checkout")]
    public IActionResult GetCheckout()
    {
      return Ok(HttpContext.Session.GetString("checkout"));
    }
    [HttpPost("checkout")]
    public void StoreCheckout([FromBody] CheckoutState data)
    {
      HttpContext.Session.SetString("checkout",
      JsonConvert.SerializeObject(data));
    }
  }
}
