using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.SpaServices.Webpack;
using rovashop.Models.Concrete;
using rovashop.Models.Abstract;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authentication.Cookies;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Antiforgery;

namespace rovashop
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
          //services.AddDbContext<RovaShopDataContext>(options => options.UseMySQL(Configuration["Data:mysql:ConnectionString"]));
          services.AddDbContext<IdentityDataContext>(options => options.UseSqlServer(Configuration["Data:Identity:ConnectionString"]));
          services.AddDbContext<RovaShopDataContext>(options => options.UseSqlServer(Configuration["Data:sqlserver:ConnectionString"]));
          //set identity
          //services.AddIdentity<IdentityUser, IdentityRole>().AddEntityFrameworkStores<IdentityDataContext>();
          services.AddDefaultIdentity<IdentityUser>().AddRoles<IdentityRole>().AddEntityFrameworkStores<IdentityDataContext>();
          services.AddMvc().AddJsonOptions(opts => {
              //autoriser le Reference looping
              opts.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Serialize;
              //ne pas sérialiser les value null
              opts.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
            });

            //pour les sessions stocké dans la bdd
            services.AddDistributedSqlServerCache(options => {
              options.ConnectionString =
              Configuration["Data:sqlserver:ConnectionString"];
              options.SchemaName = "dbo";
              options.TableName = "SessionData";
            });
            services.AddSession(options => {
              options.CookieName = "rovashop.Session";
              options.IdleTimeout = System.TimeSpan.FromHours(48);
              options.CookieHttpOnly = false;
            });

      /*services.Configure<CookiePolicyOptions>(options =>
      {
          options.CheckConsentNeeded = context => true;
          options.MinimumSameSitePolicy = SameSiteMode.None;
      });*/

          services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
            .AddCookie(options =>
            {
              options.LoginPath = "/Account/Login";
              //options.AccessDeniedPath = "/auth/accessdenied";
            })
            .AddCookie("TempCookie");
            services.AddTransient<IRepository, EFRepository>();
            services.AddTransient<IRepository, EFRepository>();
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddAntiforgery(options => {
              options.HeaderName = "X-XSRF-TOKEN";
            });
    }

        public void Configure(IApplicationBuilder app, IHostingEnvironment env,
                      ILoggerFactory loggerFactory, IServiceProvider serviceProvider)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                app.UseHsts();
            }

            app.UseDeveloperExceptionPage();
            app.UseWebpackDevMiddleware(new WebpackDevMiddlewareOptions
            {
              HotModuleReplacement = true
            });
            //if (env.IsDevelopment()) {
            // app.UseDeveloperExceptionPage();
            // app.UseBrowserLink();
            //} else {
            // app.UseExceptionHandler("/Home/Error");
            //}
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseSession();
            app.UseIdentity();
            IAntiforgery antiforgery = serviceProvider.GetService<IAntiforgery>();
            app.Use(nextDelegate => context => {
              if (context.Request.Path.StartsWithSegments("/api")
              || context.Request.Path.StartsWithSegments("/"))
              {
                context.Response.Cookies.Append("XSRF-TOKEN",
                antiforgery.GetAndStoreTokens(context).RequestToken);
              }
              return nextDelegate(context);
            });
      //SeedData.SeedDatabase(app.ApplicationServices.GetRequiredService<RovaShopDataContext>());
      //IdentitySeedData.SeedDatabase(serviceProvider);
      IdentitySeedData.SeedUsers(serviceProvider.GetService<UserManager<IdentityUser>>());
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
              routes.MapSpaFallbackRoute("angular-fallback",
              new { controller = "Home", action = "Index" });
            });
          
    }
    }
}
