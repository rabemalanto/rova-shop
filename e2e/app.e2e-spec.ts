import { RovashopPage } from './app.po';

describe('rovashop App', () => {
  let page: RovashopPage;

  beforeEach(() => {
    page = new RovashopPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
