"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var protractor_1 = require("protractor");
var RovashopPage = /** @class */ (function () {
    function RovashopPage() {
    }
    RovashopPage.prototype.navigateTo = function () {
        return protractor_1.browser.get('/');
    };
    RovashopPage.prototype.getParagraphText = function () {
        return protractor_1.element(protractor_1.by.css('app-root h1')).getText();
    };
    return RovashopPage;
}());
exports.RovashopPage = RovashopPage;
//# sourceMappingURL=app.po.js.map