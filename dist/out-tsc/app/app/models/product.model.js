"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Product = /** @class */ (function () {
    function Product(productId, name, category, description, price, supplier, rating) {
        this.productId = productId;
        this.name = name;
        this.category = category;
        this.description = description;
        this.price = price;
        this.supplier = supplier;
        this.rating = rating;
    }
    return Product;
}());
exports.Product = Product;
//# sourceMappingURL=product.model.js.map