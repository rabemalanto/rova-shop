webpackJsonp([1],{

/***/ 118:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__app_app_module__ = __webpack_require__(124);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__(135);




if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["a" /* enableProdMode */])();
}
var bootApplication = function () {
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_2__app_app_module__["a" /* AppModule */]);
};
if (true) {
    module["hot"].accept();
    module["hot"].dispose(function () {
        var oldRootElem = document.querySelector("app-root");
        var newRootElem = document.createElement("app-root");
        oldRootElem.parentNode.insertBefore(newRootElem, oldRootElem);
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().destroy();
    });
}
if (document.readyState === "complete") {
    bootApplication();
}
else {
    document.addEventListener("DOMContentLoaded", bootApplication);
}
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 121:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__admin_component__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__overview_component__ = __webpack_require__(76);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__productAdmin_component__ = __webpack_require__(77);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__orderAdmin_component__ = __webpack_require__(75);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__productEditor_component__ = __webpack_require__(122);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var AdminModule = (function () {
    function AdminModule() {
    }
    return AdminModule;
}());
AdminModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["b" /* NgModule */])({
        imports: [__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */], __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* RouterModule */], __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormsModule */]],
        declarations: [__WEBPACK_IMPORTED_MODULE_4__admin_component__["a" /* AdminComponent */], __WEBPACK_IMPORTED_MODULE_5__overview_component__["a" /* OverviewComponent */],
            __WEBPACK_IMPORTED_MODULE_6__productAdmin_component__["a" /* ProductAdminComponent */], __WEBPACK_IMPORTED_MODULE_7__orderAdmin_component__["a" /* OrderAdminComponent */], __WEBPACK_IMPORTED_MODULE_8__productEditor_component__["a" /* ProductEditorComponent */]]
    })
], AdminModule);

//# sourceMappingURL=admin.module.js.map

/***/ }),

/***/ 122:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_repository__ = __webpack_require__(6);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductEditorComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var ProductEditorComponent = (function () {
    function ProductEditorComponent(repo) {
        this.repo = repo;
    }
    Object.defineProperty(ProductEditorComponent.prototype, "product", {
        get: function () {
            return this.repo.product;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(ProductEditorComponent.prototype, "suppliers", {
        get: function () {
            return this.repo.suppliers;
        },
        enumerable: true,
        configurable: true
    });
    ProductEditorComponent.prototype.compareSuppliers = function (s1, s2) {
        return s1 && s2 && s1.name == s2.name;
    };
    return ProductEditorComponent;
}());
ProductEditorComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* Component */])({
        selector: "admin-product-editor",
        template: __webpack_require__(141)
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__models_repository__["a" /* Repository */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__models_repository__["a" /* Repository */]) === "function" && _a || Object])
], ProductEditorComponent);

var _a;
//# sourceMappingURL=productEditor.component.js.map

/***/ }),

/***/ 123:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__errorHandler_service__ = __webpack_require__(40);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

//import { Repository } from "./models/repository";
//import { Product } from "./models/product.model";
//import { Supplier } from "./models/supplier.model";

var AppComponent = (function () {
    function AppComponent(errorHandler) {
        var _this = this;
        errorHandler.errors.subscribe(function (error) {
            _this.lastError = error;
        });
    }
    Object.defineProperty(AppComponent.prototype, "error", {
        get: function () {
            return this.lastError;
        },
        enumerable: true,
        configurable: true
    });
    AppComponent.prototype.clearError = function () {
        this.lastError = null;
    };
    return AppComponent;
}());
AppComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* Component */])({
        selector: 'app-root',
        template: __webpack_require__(142),
        styles: [__webpack_require__(136)]
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__errorHandler_service__["a" /* ErrorHandlerService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__errorHandler_service__["a" /* ErrorHandlerService */]) === "function" && _a || Object])
], AppComponent);

var _a;
//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 124:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__models_model_module__ = __webpack_require__(128);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__app_component__ = __webpack_require__(123);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__app_routing__ = __webpack_require__(125);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__store_store_module__ = __webpack_require__(134);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__admin_admin_module__ = __webpack_require__(121);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__errorHandler_service__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__auth_auth_module__ = __webpack_require__(126);
/* unused harmony export handler */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};






//import { ProductTableComponent } from "./structure/productTable.component"
//import { CategoryFilterComponent } from "./structure/categoryFilter.component"
//import { ProductDetailComponent } from "./structure/productDetail.component";






var eHandler = new __WEBPACK_IMPORTED_MODULE_9__errorHandler_service__["a" /* ErrorHandlerService */]();
function handler() {
    return eHandler;
}
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["b" /* NgModule */])({
        declarations: [
            __WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* AppComponent */] //, ProductTableComponent, CategoryFilterComponent, ProductDetailComponent
        ],
        imports: [
            __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["a" /* BrowserModule */],
            __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormsModule */],
            __WEBPACK_IMPORTED_MODULE_3__angular_http__["a" /* HttpModule */],
            __WEBPACK_IMPORTED_MODULE_4__models_model_module__["a" /* ModelModule */],
            __WEBPACK_IMPORTED_MODULE_6__app_routing__["a" /* RoutingConfig */],
            __WEBPACK_IMPORTED_MODULE_7__store_store_module__["a" /* StoreModule */],
            __WEBPACK_IMPORTED_MODULE_8__admin_admin_module__["a" /* AdminModule */],
            __WEBPACK_IMPORTED_MODULE_10__auth_auth_module__["a" /* AuthModule */]
        ],
        providers: [
            { provide: __WEBPACK_IMPORTED_MODULE_9__errorHandler_service__["a" /* ErrorHandlerService */], useFactory: handler },
            { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["c" /* ErrorHandler */], useFactory: handler }
        ],
        bootstrap: [__WEBPACK_IMPORTED_MODULE_5__app_component__["a" /* AppComponent */]]
    })
], AppModule);

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 125:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_router__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__store_productSelection_component__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__store_cartDetail_component__ = __webpack_require__(81);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__store_checkout_checkoutDetail_component__ = __webpack_require__(82);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__store_checkout_checkoutPayement_component__ = __webpack_require__(83);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__store_checkout_checkoutSummary_component__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__store_checkout_orderConfirmation_component__ = __webpack_require__(85);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__admin_admin_component__ = __webpack_require__(74);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__admin_overview_component__ = __webpack_require__(76);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__admin_productAdmin_component__ = __webpack_require__(77);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__admin_orderAdmin_component__ = __webpack_require__(75);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__auth_authentication_guard__ = __webpack_require__(79);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__auth_authentication_component__ = __webpack_require__(78);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RoutingConfig; });

//import { ProductTableComponent } from "./structure/productTable.component"
//import { ProductDetailComponent } from "./structure/productDetail.component";






//admin




//login


var routes = [
    { path: "login", component: __WEBPACK_IMPORTED_MODULE_12__auth_authentication_component__["a" /* AuthenticationComponent */] },
    { path: "admin", redirectTo: "/admin/overview", pathMatch: "full" },
    //{ path: "table", component: ProductTableComponent },
    //{ path: "detail/:id", component: ProductDetailComponent },
    //{ path: "detail", component: ProductDetailComponent },
    //{ path: "", component: ProductTableComponent }]
    {
        path: "admin", component: __WEBPACK_IMPORTED_MODULE_7__admin_admin_component__["a" /* AdminComponent */],
        canActivateChild: [__WEBPACK_IMPORTED_MODULE_11__auth_authentication_guard__["a" /* AuthenticationGuard */]],
        children: [
            { path: "products", component: __WEBPACK_IMPORTED_MODULE_9__admin_productAdmin_component__["a" /* ProductAdminComponent */] },
            { path: "orders", component: __WEBPACK_IMPORTED_MODULE_10__admin_orderAdmin_component__["a" /* OrderAdminComponent */] },
            { path: "overview", component: __WEBPACK_IMPORTED_MODULE_8__admin_overview_component__["a" /* OverviewComponent */] },
            { path: "", component: __WEBPACK_IMPORTED_MODULE_8__admin_overview_component__["a" /* OverviewComponent */] }
        ]
    },
    { path: "checkout/step1", component: __WEBPACK_IMPORTED_MODULE_3__store_checkout_checkoutDetail_component__["a" /* CheckoutDetailsComponent */] },
    { path: "checkout/step2", component: __WEBPACK_IMPORTED_MODULE_4__store_checkout_checkoutPayement_component__["a" /* CheckoutPaymentComponent */] },
    { path: "checkout/step3", component: __WEBPACK_IMPORTED_MODULE_5__store_checkout_checkoutSummary_component__["a" /* CheckoutSummaryComponent */] },
    { path: "checkout/confirmation", component: __WEBPACK_IMPORTED_MODULE_6__store_checkout_orderConfirmation_component__["a" /* OrderConfirmationComponent */] },
    { path: "checkout", component: __WEBPACK_IMPORTED_MODULE_3__store_checkout_checkoutDetail_component__["a" /* CheckoutDetailsComponent */] },
    { path: "cart", component: __WEBPACK_IMPORTED_MODULE_2__store_cartDetail_component__["a" /* CartDetailComponent */] },
    { path: "store", component: __WEBPACK_IMPORTED_MODULE_1__store_productSelection_component__["a" /* ProductSelectionComponent */] },
    { path: "", component: __WEBPACK_IMPORTED_MODULE_1__store_productSelection_component__["a" /* ProductSelectionComponent */] }
];
var RoutingConfig = __WEBPACK_IMPORTED_MODULE_0__angular_router__["a" /* RouterModule */].forRoot(routes);
//# sourceMappingURL=app.routing.js.map

/***/ }),

/***/ 126:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__authentication_service__ = __webpack_require__(30);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__authentication_component__ = __webpack_require__(78);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__authentication_guard__ = __webpack_require__(79);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};







var AuthModule = (function () {
    function AuthModule() {
    }
    return AuthModule;
}());
AuthModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["b" /* NgModule */])({
        imports: [__WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* RouterModule */], __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormsModule */], __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__["a" /* BrowserModule */]],
        declarations: [__WEBPACK_IMPORTED_MODULE_5__authentication_component__["a" /* AuthenticationComponent */]],
        providers: [__WEBPACK_IMPORTED_MODULE_4__authentication_service__["a" /* AuthenticationService */], __WEBPACK_IMPORTED_MODULE_6__authentication_guard__["a" /* AuthenticationGuard */]],
        exports: [__WEBPACK_IMPORTED_MODULE_5__authentication_component__["a" /* AuthenticationComponent */]]
    })
], AuthModule);

//# sourceMappingURL=auth.module.js.map

/***/ }),

/***/ 127:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Filter; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return Pagination; });
var Filter = (function () {
    function Filter() {
        this.related = false;
    }
    Filter.prototype.reset = function () {
        this.category = this.search = null;
        this.related = false;
    };
    return Filter;
}());

var Pagination = (function () {
    function Pagination() {
        this.productsPerPage = 6;
        this.currentPage = 1;
    }
    return Pagination;
}());

//# sourceMappingURL=configClasses.repository.js.map

/***/ }),

/***/ 128:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__repository__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__cart_model__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__order_model__ = __webpack_require__(24);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ModelModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var ModelModule = (function () {
    function ModelModule() {
    }
    return ModelModule;
}());
ModelModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["b" /* NgModule */])({
        providers: [__WEBPACK_IMPORTED_MODULE_1__repository__["a" /* Repository */], __WEBPACK_IMPORTED_MODULE_2__cart_model__["a" /* Cart */], __WEBPACK_IMPORTED_MODULE_3__order_model__["a" /* Order */]]
    })
], ModelModule);

//# sourceMappingURL=model.module.js.map

/***/ }),

/***/ 129:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_cart_model__ = __webpack_require__(23);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CartSummaryComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var CartSummaryComponent = (function () {
    function CartSummaryComponent(cart) {
        this.cart = cart;
    }
    Object.defineProperty(CartSummaryComponent.prototype, "itemCount", {
        get: function () {
            return this.cart.itemCount;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CartSummaryComponent.prototype, "totalPrice", {
        get: function () {
            return this.cart.totalPrice;
        },
        enumerable: true,
        configurable: true
    });
    return CartSummaryComponent;
}());
CartSummaryComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* Component */])({
        selector: "store-cartsummary",
        template: __webpack_require__(145)
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__models_cart_model__["a" /* Cart */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__models_cart_model__["a" /* Cart */]) === "function" && _a || Object])
], CartSummaryComponent);

var _a;
//# sourceMappingURL=cartSummary.component.js.map

/***/ }),

/***/ 130:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_repository__ = __webpack_require__(6);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CategoryFilterComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var CategoryFilterComponent = (function () {
    function CategoryFilterComponent(repo) {
        this.repo = repo;
    }
    Object.defineProperty(CategoryFilterComponent.prototype, "categories", {
        get: function () {
            return this.repo.categories;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(CategoryFilterComponent.prototype, "currentCategory", {
        get: function () {
            return this.repo.filter.category;
        },
        enumerable: true,
        configurable: true
    });
    CategoryFilterComponent.prototype.setCurrentCategory = function (newCategory) {
        this.repo.filter.category = newCategory;
        this.repo.getProducts();
    };
    return CategoryFilterComponent;
}());
CategoryFilterComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* Component */])({
        selector: "store-categoryfilter",
        template: __webpack_require__(146)
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__models_repository__["a" /* Repository */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__models_repository__["a" /* Repository */]) === "function" && _a || Object])
], CategoryFilterComponent);

var _a;
//# sourceMappingURL=categoryFilter.component.js.map

/***/ }),

/***/ 131:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_repository__ = __webpack_require__(6);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PaginationComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PaginationComponent = (function () {
    function PaginationComponent(repo) {
        this.repo = repo;
    }
    Object.defineProperty(PaginationComponent.prototype, "pages", {
        get: function () {
            if (this.repo.products != null) {
                return Array(Math.ceil(this.repo.products.length
                    / this.repo.pagination.productsPerPage))
                    .fill(0).map(function (x, i) { return i + 1; });
            }
            else {
                return [];
            }
        },
        enumerable: true,
        configurable: true
    });
    PaginationComponent.prototype.changePage = function (newPage) {
        this.repo.pagination.currentPage = newPage;
    };
    return PaginationComponent;
}());
PaginationComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* Component */])({
        selector: "store-pagination",
        template: __webpack_require__(151)
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__models_repository__["a" /* Repository */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__models_repository__["a" /* Repository */]) === "function" && _a || Object])
], PaginationComponent);

var _a;
//# sourceMappingURL=pagination.component.js.map

/***/ }),

/***/ 132:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_repository__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__models_cart_model__ = __webpack_require__(23);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductListComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ProductListComponent = (function () {
    function ProductListComponent(repo, cart) {
        this.repo = repo;
        this.cart = cart;
    }
    Object.defineProperty(ProductListComponent.prototype, "products", {
        get: function () {
            if (this.repo.products != null && this.repo.products.length > 0) {
                var pageIndex = (this.repo.pagination.currentPage - 1)
                    * this.repo.pagination.productsPerPage;
                return this.repo.products.slice(pageIndex, pageIndex + this.repo.pagination.productsPerPage);
            }
        },
        enumerable: true,
        configurable: true
    });
    ProductListComponent.prototype.addToCart = function (product) {
        this.cart.addProduct(product);
    };
    return ProductListComponent;
}());
ProductListComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* Component */])({
        selector: "store-product-list",
        template: __webpack_require__(152)
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__models_repository__["a" /* Repository */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__models_repository__["a" /* Repository */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__models_cart_model__["a" /* Cart */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__models_cart_model__["a" /* Cart */]) === "function" && _b || Object])
], ProductListComponent);

var _a, _b;
//# sourceMappingURL=productList.component.js.map

/***/ }),

/***/ 133:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__models_product_model__ = __webpack_require__(80);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RatingsComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var RatingsComponent = (function () {
    function RatingsComponent() {
    }
    Object.defineProperty(RatingsComponent.prototype, "stars", {
        get: function () {
            if (this.product != null && this.product.rating != null) {
                var total = this.product.rating.map(function (r) { return r.stars; })
                    .reduce(function (prev, curr) { return prev + curr; }, 0);
                var count_1 = Math.round(total / this.product.rating.length);
                return Array(5).fill(false).map(function (value, index) {
                    return index < count_1;
                });
            }
            else {
                return [];
            }
        },
        enumerable: true,
        configurable: true
    });
    return RatingsComponent;
}());
__decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["o" /* Input */])(),
    __metadata("design:type", typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_0__models_product_model__["a" /* Product */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_0__models_product_model__["a" /* Product */]) === "function" && _a || Object)
], RatingsComponent.prototype, "product", void 0);
RatingsComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["_11" /* Component */])({
        selector: "store-ratings",
        template: __webpack_require__(154)
    })
], RatingsComponent);

var _a;
//# sourceMappingURL=rating.component.js.map

/***/ }),

/***/ 134:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__cartSummary_component__ = __webpack_require__(129);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__categoryFilter_component__ = __webpack_require__(130);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pagination_component__ = __webpack_require__(131);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__productList_component__ = __webpack_require__(132);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__rating_component__ = __webpack_require__(133);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__productSelection_component__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__cartDetail_component__ = __webpack_require__(81);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__angular_router__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__angular_forms__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__checkout_checkoutDetail_component__ = __webpack_require__(82);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__checkout_checkoutPayement_component__ = __webpack_require__(83);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__checkout_checkoutSummary_component__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__checkout_orderConfirmation_component__ = __webpack_require__(85);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return StoreModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};















var StoreModule = (function () {
    function StoreModule() {
    }
    return StoreModule;
}());
StoreModule = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["b" /* NgModule */])({
        declarations: [__WEBPACK_IMPORTED_MODULE_2__cartSummary_component__["a" /* CartSummaryComponent */], __WEBPACK_IMPORTED_MODULE_3__categoryFilter_component__["a" /* CategoryFilterComponent */],
            __WEBPACK_IMPORTED_MODULE_4__pagination_component__["a" /* PaginationComponent */], __WEBPACK_IMPORTED_MODULE_5__productList_component__["a" /* ProductListComponent */], __WEBPACK_IMPORTED_MODULE_6__rating_component__["a" /* RatingsComponent */],
            __WEBPACK_IMPORTED_MODULE_7__productSelection_component__["a" /* ProductSelectionComponent */], __WEBPACK_IMPORTED_MODULE_8__cartDetail_component__["a" /* CartDetailComponent */], __WEBPACK_IMPORTED_MODULE_11__checkout_checkoutDetail_component__["a" /* CheckoutDetailsComponent */],
            __WEBPACK_IMPORTED_MODULE_12__checkout_checkoutPayement_component__["a" /* CheckoutPaymentComponent */], __WEBPACK_IMPORTED_MODULE_13__checkout_checkoutSummary_component__["a" /* CheckoutSummaryComponent */], __WEBPACK_IMPORTED_MODULE_14__checkout_orderConfirmation_component__["a" /* OrderConfirmationComponent */]
        ],
        imports: [__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */], __WEBPACK_IMPORTED_MODULE_9__angular_router__["a" /* RouterModule */], __WEBPACK_IMPORTED_MODULE_10__angular_forms__["a" /* FormsModule */]],
        exports: [__WEBPACK_IMPORTED_MODULE_7__productSelection_component__["a" /* ProductSelectionComponent */]]
    })
], StoreModule);

//# sourceMappingURL=store.module.js.map

/***/ }),

/***/ 135:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.
// The file contents for the current environment will overwrite these during build.
var environment = {
    production: false
};
//# sourceMappingURL=environment.js.map

/***/ }),

/***/ 136:
/***/ (function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(59)(false);
// imports


// module
exports.push([module.i, "", ""]);

// exports


/*** EXPORTS FROM exports-loader ***/
module.exports = module.exports.toString();

/***/ }),

/***/ 137:
/***/ (function(module, exports) {

module.exports = "<div class=\"wrapper\">\r\n  <!-- BEGIN HEADER -->\r\n  <header class=\"page-header\">\r\n    <nav class=\"navbar mega-menu\" role=\"navigation\">\r\n      <div class=\"container-fluid\">\r\n        <div class=\"clearfix navbar-fixed-top\">\r\n          <!-- Brand and toggle get grouped for better mobile display -->\r\n          <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-responsive-collapse\">\r\n            <span class=\"sr-only\">navigation</span>\r\n            <span class=\"toggle-icon\">\r\n              <span class=\"icon-bar\"></span>\r\n              <span class=\"icon-bar\"></span>\r\n              <span class=\"icon-bar\"></span>\r\n            </span>\r\n          </button>\r\n          <!-- End Toggle Button -->\r\n          <!-- BEGIN LOGO -->\r\n          <a id=\"index\" class=\"page-logo\" href=\"#\" routerLink=\"/admin\">\r\n            <!--img src=\"../assets/layouts/layout5/img/logo.png\" alt=\"Logo\"-->\r\n            Acceuil\r\n          </a>\r\n          <!-- END LOGO -->\r\n          <!-- BEGIN SEARCH -->\r\n          <form class=\"search\" action=\"extra_search.html\" method=\"GET\">\r\n            <input type=\"name\" class=\"form-control\" name=\"query\" placeholder=\"Search...\">\r\n            <a href=\"javascript:;\" class=\"btn submit md-skip\">\r\n              <i class=\"fa fa-search\"></i>\r\n            </a>\r\n          </form>\r\n          <!-- END SEARCH -->\r\n          <!-- BEGIN TOPBAR ACTIONS -->\r\n          <div class=\"topbar-actions\">\r\n            <!-- BEGIN QUICK SIDEBAR TOGGLER -->\r\n            <button type=\"button\" class=\"quick-sidebar-toggler\" (click)=\"authService.logout()\">\r\n              <span class=\"sr-only\">Toggle Quick Sidebar</span>\r\n              <i class=\"icon-logout\"></i>\r\n            </button>\r\n            <!-- END QUICK SIDEBAR TOGGLER -->\r\n          </div>\r\n          <!-- END TOPBAR ACTIONS -->\r\n        </div>\r\n        <!-- BEGIN HEADER MENU -->\r\n        <div class=\"nav-collapse collapse navbar-collapse navbar-responsive-collapse\">\r\n          <ul class=\"nav navbar-nav\">\r\n            <li class=\"dropdown dropdown-fw dropdown-fw-disabled\" routerLink=\"/admin/overview\" routerLinkActive=\"active open selected\" [routerLinkActiveOptions]=\"{exact: true}\">\r\n              <a href=\"javascript:;\" class=\"text-uppercase\">\r\n                <i class=\"icon-home\"></i> Tableau de bord\r\n              </a>\r\n            </li>\r\n            <li class=\"dropdown dropdown-fw dropdown-fw-disabled\" routerLink=\"/admin/products\" routerLinkActive=\"active open selected\">\r\n              <a href=\"javascript:;\" class=\"text-uppercase\">\r\n                <i class=\"icon-puzzle\"></i> Chef produit\r\n              </a>\r\n            </li>\r\n            <li class=\"dropdown dropdown-fw dropdown-fw-disabled\" routerLink=\"/admin/orders\" routerLinkActive=\"active open selected\">\r\n              <a href=\"javascript:;\" class=\"text-uppercase\">\r\n                <i class=\"icon-briefcase\"></i> Commandes\r\n              </a>\r\n            </li>\r\n          </ul>\r\n        </div>\r\n        <!-- END HEADER MENU -->\r\n      </div>\r\n      <!--/container-->\r\n    </nav>\r\n  </header>\r\n  <div class=\"\">\r\n    <div class=\"page-content\" style=\"padding-top:0px;\">\r\n      <router-outlet></router-outlet>\r\n    </div>\r\n    </div>\r\n      <!-- END HEADER -->\r\n    </div>\r\n"

/***/ }),

/***/ 138:
/***/ (function(module, exports) {

module.exports = "<table *ngIf=\"orders?.length > 0; else nodata\" class=\"table table-striped\">\r\n  <tr>\r\n    <th>Customer</th>\r\n    <th>Address</th>\r\n    <th>Products</th>\r\n    <th>Total</th>\r\n    <th></th>\r\n  </tr>\r\n  <tr *ngFor=\"let o of orders\">\r\n    <td>{{o.name}}</td>\r\n    <td>{{o.address}}</td>\r\n    <td>{{o.products.length}}</td>\r\n    <td>{{o.payment.total | currency:EUR:true}}</td>\r\n    <td *ngIf=\"!o.shipped; else shipped\">\r\n      <button class=\"btn btn-sm btn-primary\"\r\n              (click)=\"markShipped(o)\">\r\n        Ship\r\n      </button>\r\n    </td>\r\n  </tr>\r\n</table>\r\n\r\n<ng-template #shipped>\r\n  <td>Shipped</td>\r\n</ng-template>\r\n<ng-template #nodata>\r\n  <h3 class=\"text-center\">There are no orders</h3>\r\n</ng-template>\r\n"

/***/ }),

/***/ 139:
/***/ (function(module, exports) {

module.exports = "      <!-- BEGIN PAGE BASE CONTENT -->\r\n      <div class=\"row\">\r\n        <div class=\"col-lg-4 col-md-4 col-sm-6 col-xs-12 margin-bottom-10\">\r\n          <div class=\"dashboard-stat blue\">\r\n            <div class=\"visual\">\r\n              <i class=\"fa fa-briefcase fa-icon-medium\"></i>\r\n            </div>\r\n            <div class=\"details\">\r\n              <div class=\"number\"> $168,492.54 </div>\r\n              <div class=\"desc\"> Lifetime Sales </div>\r\n            </div>\r\n            <a class=\"more\" href=\"javascript:;\">\r\n              View more\r\n              <i class=\"m-icon-swapright m-icon-white\"></i>\r\n            </a>\r\n          </div>\r\n        </div>\r\n        <div class=\"col-lg-4 col-md-4 col-sm-6 col-xs-12\">\r\n          <div class=\"dashboard-stat red\">\r\n            <div class=\"visual\">\r\n              <i class=\"fa fa-shopping-cart\"></i>\r\n            </div>\r\n            <div class=\"details\">\r\n              <div class=\"number\"> There are {{orders?.length || 0}} </div>\r\n              <div class=\"desc\"> Total Orders </div>\r\n            </div>\r\n            <a class=\"more\" href=\"javascript:;\">\r\n              View more\r\n              <i class=\"m-icon-swapright m-icon-white\"></i>\r\n            </a>\r\n          </div>\r\n        </div>\r\n        <div class=\"col-lg-4 col-md-4 col-sm-6 col-xs-12\">\r\n          <div class=\"dashboard-stat green\">\r\n            <div class=\"visual\">\r\n              <i class=\"fa fa-group fa-icon-medium\"></i>\r\n            </div>\r\n            <div class=\"details\">\r\n              <div class=\"number\"> $670.54 </div>\r\n              <div class=\"desc\"> Average Orders </div>\r\n            </div>\r\n            <a class=\"more\" href=\"javascript:;\">\r\n              View more\r\n              <i class=\"m-icon-swapright m-icon-white\"></i>\r\n            </a>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <div class=\"row\">\r\n        <div class=\"col-md-6\">\r\n          <!-- Begin: life time stats -->\r\n          <div class=\"portlet light bordered\">\r\n            <div class=\"portlet-title\">\r\n              <div class=\"caption\">\r\n                <i class=\"icon-share font-blue\"></i>\r\n                <span class=\"caption-subject font-blue bold uppercase\">Overview</span>\r\n                <span class=\"caption-helper\">report overview...</span>\r\n              </div>\r\n              <div class=\"actions\">\r\n                <div class=\"btn-group\">\r\n                  <a class=\"btn green btn-circle btn-sm\" href=\"javascript:;\" data-toggle=\"dropdown\" data-hover=\"dropdown\" data-close-others=\"true\">\r\n                    Actions\r\n                    <i class=\"fa fa-angle-down\"></i>\r\n                  </a>\r\n                  <ul class=\"dropdown-menu pull-right\">\r\n                    <li>\r\n                      <a href=\"javascript:;\"> All Project </a>\r\n                    </li>\r\n                    <li class=\"divider\"> </li>\r\n                    <li>\r\n                      <a href=\"javascript:;\"> AirAsia </a>\r\n                    </li>\r\n                    <li>\r\n                      <a href=\"javascript:;\"> Cruise </a>\r\n                    </li>\r\n                    <li>\r\n                      <a href=\"javascript:;\"> HSBC </a>\r\n                    </li>\r\n                    <li class=\"divider\"> </li>\r\n                    <li>\r\n                      <a href=\"javascript:;\">\r\n                        Pending\r\n                        <span class=\"badge badge-danger\"> 4 </span>\r\n                      </a>\r\n                    </li>\r\n                    <li>\r\n                      <a href=\"javascript:;\">\r\n                        Completed\r\n                        <span class=\"badge badge-success\"> 12 </span>\r\n                      </a>\r\n                    </li>\r\n                    <li>\r\n                      <a href=\"javascript:;\">\r\n                        Overdue\r\n                        <span class=\"badge badge-warning\"> 9 </span>\r\n                      </a>\r\n                    </li>\r\n                  </ul>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"portlet-body\">\r\n              <div class=\"tabbable-line\">\r\n                <ul class=\"nav nav-tabs\">\r\n                  <li class=\"active\">\r\n                    <a href=\"#overview_1\" data-toggle=\"tab\"> Top Selling </a>\r\n                  </li>\r\n                  <li>\r\n                    <a href=\"#overview_2\" data-toggle=\"tab\"> Most Viewed </a>\r\n                  </li>\r\n                  <li>\r\n                    <a href=\"#overview_3\" data-toggle=\"tab\"> New Customers </a>\r\n                  </li>\r\n                  <li class=\"dropdown\">\r\n                    <a href=\"javascript:;\" class=\"dropdown-toggle\" data-toggle=\"dropdown\">\r\n                      Orders\r\n                      <i class=\"fa fa-angle-down\"></i>\r\n                    </a>\r\n                    <ul class=\"dropdown-menu pull-right\">\r\n                      <li>\r\n                        <a href=\"#overview_4\" data-toggle=\"tab\">\r\n                          <i class=\"icon-bell\"></i> Latest 10 Orders\r\n                        </a>\r\n                      </li>\r\n                      <li>\r\n                        <a href=\"#overview_4\" data-toggle=\"tab\">\r\n                          <i class=\"icon-info\"></i> Pending Orders\r\n                        </a>\r\n                      </li>\r\n                      <li>\r\n                        <a href=\"#overview_4\" data-toggle=\"tab\">\r\n                          <i class=\"icon-speech\"></i> Completed Orders\r\n                        </a>\r\n                      </li>\r\n                      <li class=\"divider\"></li>\r\n                      <li>\r\n                        <a href=\"#overview_4\" data-toggle=\"tab\">\r\n                          <i class=\"icon-settings\"></i> Rejected Orders\r\n                        </a>\r\n                      </li>\r\n                    </ul>\r\n                  </li>\r\n                </ul>\r\n                <div class=\"tab-content\">\r\n                  <div class=\"tab-pane active\" id=\"overview_1\">\r\n                    <div class=\"table-responsive\">\r\n                      <table class=\"table table-striped table-hover table-bordered\">\r\n                        <thead>\r\n                          <tr>\r\n                            <th> Product Name </th>\r\n                            <th> Price </th>\r\n                            <th> Sold </th>\r\n                            <th> </th>\r\n                          </tr>\r\n                        </thead>\r\n                        <tbody>\r\n                          <tr>\r\n                            <td>\r\n                              <a href=\"javascript:;\"> Apple iPhone 4s - 16GB - Black </a>\r\n                            </td>\r\n                            <td> $625.50 </td>\r\n                            <td> 809 </td>\r\n                            <td>\r\n                              <a href=\"javascript:;\" class=\"btn btn-sm btn-default\">\r\n                                <i class=\"fa fa-search\"></i> View\r\n                              </a>\r\n                            </td>\r\n                          </tr>\r\n                          <tr>\r\n                            <td>\r\n                              <a href=\"javascript:;\"> Samsung Galaxy S III SGH-I747 - 16GB </a>\r\n                            </td>\r\n                            <td> $915.50 </td>\r\n                            <td> 6709 </td>\r\n                            <td>\r\n                              <a href=\"javascript:;\" class=\"btn btn-sm btn-default\">\r\n                                <i class=\"fa fa-search\"></i> View\r\n                              </a>\r\n                            </td>\r\n                          </tr>\r\n                          <tr>\r\n                            <td>\r\n                              <a href=\"javascript:;\"> Motorola Droid 4 XT894 - 16GB - Black </a>\r\n                            </td>\r\n                            <td> $878.50 </td>\r\n                            <td> 784 </td>\r\n                            <td>\r\n                              <a href=\"javascript:;\" class=\"btn btn-sm btn-default\">\r\n                                <i class=\"fa fa-search\"></i> View\r\n                              </a>\r\n                            </td>\r\n                          </tr>\r\n                          <tr>\r\n                            <td>\r\n                              <a href=\"javascript:;\"> Samsung Galaxy Note 4 </a>\r\n                            </td>\r\n                            <td> $925.50 </td>\r\n                            <td> 21245 </td>\r\n                            <td>\r\n                              <a href=\"javascript:;\" class=\"btn btn-sm btn-default\">\r\n                                <i class=\"fa fa-search\"></i> View\r\n                              </a>\r\n                            </td>\r\n                          </tr>\r\n                          <tr>\r\n                            <td>\r\n                              <a href=\"javascript:;\"> Regatta Luca 3 in 1 Jacket </a>\r\n                            </td>\r\n                            <td> $25.50 </td>\r\n                            <td> 1245 </td>\r\n                            <td>\r\n                              <a href=\"javascript:;\" class=\"btn btn-sm btn-default\">\r\n                                <i class=\"fa fa-search\"></i> View\r\n                              </a>\r\n                            </td>\r\n                          </tr>\r\n                          <tr>\r\n                            <td>\r\n                              <a href=\"javascript:;\"> Samsung Galaxy Note 3 </a>\r\n                            </td>\r\n                            <td> $925.50 </td>\r\n                            <td> 21245 </td>\r\n                            <td>\r\n                              <a href=\"javascript:;\" class=\"btn btn-sm btn-default\">\r\n                                <i class=\"fa fa-search\"></i> View\r\n                              </a>\r\n                            </td>\r\n                          </tr>\r\n                        </tbody>\r\n                      </table>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"tab-pane\" id=\"overview_2\">\r\n                    <div class=\"table-responsive\">\r\n                      <table class=\"table table-striped table-hover table-bordered\">\r\n                        <thead>\r\n                          <tr>\r\n                            <th> Product Name </th>\r\n                            <th> Price </th>\r\n                            <th> Views </th>\r\n                            <th> </th>\r\n                          </tr>\r\n                        </thead>\r\n                        <tbody>\r\n                          <tr>\r\n                            <td>\r\n                              <a href=\"javascript:;\"> Metronic - Responsive Admin + Frontend Theme </a>\r\n                            </td>\r\n                            <td> $20.00 </td>\r\n                            <td> 11190 </td>\r\n                            <td>\r\n                              <a href=\"javascript:;\" class=\"btn btn-sm btn-default\">\r\n                                <i class=\"fa fa-search\"></i> View\r\n                              </a>\r\n                            </td>\r\n                          </tr>\r\n                          <tr>\r\n                            <td>\r\n                              <a href=\"javascript:;\"> Regatta Luca 3 in 1 Jacket </a>\r\n                            </td>\r\n                            <td> $25.50 </td>\r\n                            <td> 1245 </td>\r\n                            <td>\r\n                              <a href=\"javascript:;\" class=\"btn btn-sm btn-default\">\r\n                                <i class=\"fa fa-search\"></i> View\r\n                              </a>\r\n                            </td>\r\n                          </tr>\r\n                          <tr>\r\n                            <td>\r\n                              <a href=\"javascript:;\"> Motorola Droid 4 XT894 - 16GB - Black </a>\r\n                            </td>\r\n                            <td> $878.50 </td>\r\n                            <td> 784 </td>\r\n                            <td>\r\n                              <a href=\"javascript:;\" class=\"btn btn-sm btn-default\">\r\n                                <i class=\"fa fa-search\"></i> View\r\n                              </a>\r\n                            </td>\r\n                          </tr>\r\n                          <tr>\r\n                            <td>\r\n                              <a href=\"javascript:;\"> Apple iPhone 4s - 16GB - Black </a>\r\n                            </td>\r\n                            <td> $625.50 </td>\r\n                            <td> 809 </td>\r\n                            <td>\r\n                              <a href=\"javascript:;\" class=\"btn btn-sm btn-default\">\r\n                                <i class=\"fa fa-search\"></i> View\r\n                              </a>\r\n                            </td>\r\n                          </tr>\r\n                          <tr>\r\n                            <td>\r\n                              <a href=\"javascript:;\"> Samsung Galaxy S III SGH-I747 - 16GB </a>\r\n                            </td>\r\n                            <td> $915.50 </td>\r\n                            <td> 6709 </td>\r\n                            <td>\r\n                              <a href=\"javascript:;\" class=\"btn btn-sm btn-default\">\r\n                                <i class=\"fa fa-search\"></i> View\r\n                              </a>\r\n                            </td>\r\n                          </tr>\r\n                          <tr>\r\n                            <td>\r\n                              <a href=\"javascript:;\"> Motorola Droid 4 XT894 - 16GB - Black </a>\r\n                            </td>\r\n                            <td> $878.50 </td>\r\n                            <td> 784 </td>\r\n                            <td>\r\n                              <a href=\"javascript:;\" class=\"btn btn-sm btn-default\">\r\n                                <i class=\"fa fa-search\"></i> View\r\n                              </a>\r\n                            </td>\r\n                          </tr>\r\n                        </tbody>\r\n                      </table>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"tab-pane\" id=\"overview_3\">\r\n                    <div class=\"table-responsive\">\r\n                      <table class=\"table table-striped table-hover table-bordered\">\r\n                        <thead>\r\n                          <tr>\r\n                            <th> Customer Name </th>\r\n                            <th> Total Orders </th>\r\n                            <th> Total Amount </th>\r\n                            <th> </th>\r\n                          </tr>\r\n                        </thead>\r\n                        <tbody>\r\n                          <tr>\r\n                            <td>\r\n                              <a href=\"javascript:;\"> David Wilson </a>\r\n                            </td>\r\n                            <td> 3 </td>\r\n                            <td> $625.50 </td>\r\n                            <td>\r\n                              <a href=\"javascript:;\" class=\"btn btn-sm btn-default\">\r\n                                <i class=\"fa fa-search\"></i> View\r\n                              </a>\r\n                            </td>\r\n                          </tr>\r\n                          <tr>\r\n                            <td>\r\n                              <a href=\"javascript:;\"> Amanda Nilson </a>\r\n                            </td>\r\n                            <td> 4 </td>\r\n                            <td> $12625.50 </td>\r\n                            <td>\r\n                              <a href=\"javascript:;\" class=\"btn btn-sm btn-default\">\r\n                                <i class=\"fa fa-search\"></i> View\r\n                              </a>\r\n                            </td>\r\n                          </tr>\r\n                          <tr>\r\n                            <td>\r\n                              <a href=\"javascript:;\"> Paul Strong </a>\r\n                            </td>\r\n                            <td> 1 </td>\r\n                            <td> $890.85 </td>\r\n                            <td>\r\n                              <a href=\"javascript:;\" class=\"btn btn-sm btn-default\">\r\n                                <i class=\"fa fa-search\"></i> View\r\n                              </a>\r\n                            </td>\r\n                          </tr>\r\n                          <tr>\r\n                            <td>\r\n                              <a href=\"javascript:;\"> Jhon Doe </a>\r\n                            </td>\r\n                            <td> 2 </td>\r\n                            <td> $125.00 </td>\r\n                            <td>\r\n                              <a href=\"javascript:;\" class=\"btn btn-sm btn-default\">\r\n                                <i class=\"fa fa-search\"></i> View\r\n                              </a>\r\n                            </td>\r\n                          </tr>\r\n                          <tr>\r\n                            <td>\r\n                              <a href=\"javascript:;\"> Bill Chang </a>\r\n                            </td>\r\n                            <td> 45 </td>\r\n                            <td> $12,125.70 </td>\r\n                            <td>\r\n                              <a href=\"javascript:;\" class=\"btn btn-sm btn-default\">\r\n                                <i class=\"fa fa-search\"></i> View\r\n                              </a>\r\n                            </td>\r\n                          </tr>\r\n                          <tr>\r\n                            <td>\r\n                              <a href=\"javascript:;\"> Paul Strong </a>\r\n                            </td>\r\n                            <td> 1 </td>\r\n                            <td> $890.85 </td>\r\n                            <td>\r\n                              <a href=\"javascript:;\" class=\"btn btn-sm btn-default\">\r\n                                <i class=\"fa fa-search\"></i> View\r\n                              </a>\r\n                            </td>\r\n                          </tr>\r\n                        </tbody>\r\n                      </table>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"tab-pane\" id=\"overview_4\">\r\n                    <div class=\"table-responsive\">\r\n                      <table class=\"table table-striped table-hover table-bordered\">\r\n                        <thead>\r\n                          <tr>\r\n                            <th> Customer Name </th>\r\n                            <th> Date </th>\r\n                            <th> Amount </th>\r\n                            <th> Status </th>\r\n                            <th> </th>\r\n                          </tr>\r\n                        </thead>\r\n                        <tbody>\r\n                          <tr>\r\n                            <td>\r\n                              <a href=\"javascript:;\"> David Wilson </a>\r\n                            </td>\r\n                            <td> 3 Jan, 2013 </td>\r\n                            <td> $625.50 </td>\r\n                            <td>\r\n                              <span class=\"label label-sm label-warning\"> Pending </span>\r\n                            </td>\r\n                            <td>\r\n                              <a href=\"javascript:;\" class=\"btn btn-sm btn-default\">\r\n                                <i class=\"fa fa-search\"></i> View\r\n                              </a>\r\n                            </td>\r\n                          </tr>\r\n                          <tr>\r\n                            <td>\r\n                              <a href=\"javascript:;\"> Amanda Nilson </a>\r\n                            </td>\r\n                            <td> 13 Feb, 2013 </td>\r\n                            <td> $12625.50 </td>\r\n                            <td>\r\n                              <span class=\"label label-sm label-warning\"> Pending </span>\r\n                            </td>\r\n                            <td>\r\n                              <a href=\"javascript:;\" class=\"btn btn-sm btn-default\">\r\n                                <i class=\"fa fa-search\"></i> View\r\n                              </a>\r\n                            </td>\r\n                          </tr>\r\n                          <tr>\r\n                            <td>\r\n                              <a href=\"javascript:;\"> Paul Strong </a>\r\n                            </td>\r\n                            <td> 1 Jun, 2013 </td>\r\n                            <td> $890.85 </td>\r\n                            <td>\r\n                              <span class=\"label label-sm label-success\"> Success </span>\r\n                            </td>\r\n                            <td>\r\n                              <a href=\"javascript:;\" class=\"btn btn-sm btn-default\">\r\n                                <i class=\"fa fa-search\"></i> View\r\n                              </a>\r\n                            </td>\r\n                          </tr>\r\n                          <tr>\r\n                            <td>\r\n                              <a href=\"javascript:;\"> Jhon Doe </a>\r\n                            </td>\r\n                            <td> 20 Mar, 2013 </td>\r\n                            <td> $125.00 </td>\r\n                            <td>\r\n                              <span class=\"label label-sm label-success\"> Success </span>\r\n                            </td>\r\n                            <td>\r\n                              <a href=\"javascript:;\" class=\"btn btn-sm btn-default\">\r\n                                <i class=\"fa fa-search\"></i> View\r\n                              </a>\r\n                            </td>\r\n                          </tr>\r\n                          <tr>\r\n                            <td>\r\n                              <a href=\"javascript:;\"> Bill Chang </a>\r\n                            </td>\r\n                            <td> 29 May, 2013 </td>\r\n                            <td> $12,125.70 </td>\r\n                            <td>\r\n                              <span class=\"label label-sm label-info\"> In Process </span>\r\n                            </td>\r\n                            <td>\r\n                              <a href=\"javascript:;\" class=\"btn btn-sm btn-default\">\r\n                                <i class=\"fa fa-search\"></i> View\r\n                              </a>\r\n                            </td>\r\n                          </tr>\r\n                          <tr>\r\n                            <td>\r\n                              <a href=\"javascript:;\"> Paul Strong </a>\r\n                            </td>\r\n                            <td> 1 Jun, 2013 </td>\r\n                            <td> $890.85 </td>\r\n                            <td>\r\n                              <span class=\"label label-sm label-success\"> Success </span>\r\n                            </td>\r\n                            <td>\r\n                              <a href=\"javascript:;\" class=\"btn btn-sm btn-default\">\r\n                                <i class=\"fa fa-search\"></i> View\r\n                              </a>\r\n                            </td>\r\n                          </tr>\r\n                        </tbody>\r\n                      </table>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <!-- End: life time stats -->\r\n        </div>\r\n        <div class=\"col-md-6\">\r\n          <!-- Begin: life time stats -->\r\n          <!-- BEGIN PORTLET-->\r\n          <div class=\"portlet light bordered\">\r\n            <div class=\"portlet-title tabbable-line\">\r\n              <div class=\"caption\">\r\n                <i class=\"icon-globe font-red\"></i>\r\n                <span class=\"caption-subject font-red bold uppercase\">Revenue</span>\r\n              </div>\r\n              <ul class=\"nav nav-tabs\">\r\n                <li class=\"active\">\r\n                  <a href=\"#portlet_ecommerce_tab_1\" data-toggle=\"tab\"> Amounts </a>\r\n                </li>\r\n                <li>\r\n                  <a href=\"#portlet_ecommerce_tab_2\" id=\"statistics_orders_tab\" data-toggle=\"tab\"> Orders </a>\r\n                </li>\r\n              </ul>\r\n            </div>\r\n            <div class=\"portlet-body\">\r\n              <div class=\"tab-content\">\r\n                <div class=\"tab-pane active\" id=\"portlet_ecommerce_tab_1\">\r\n                  <div id=\"statistics_1\" class=\"chart\"> </div>\r\n                </div>\r\n                <div class=\"tab-pane\" id=\"portlet_ecommerce_tab_2\">\r\n                  <div id=\"statistics_2\" class=\"chart\"> </div>\r\n                </div>\r\n              </div>\r\n              <div class=\"well margin-top-20\">\r\n                <div class=\"row\">\r\n                  <div class=\"col-md-3 col-sm-3 col-xs-6 text-stat\">\r\n                    <span class=\"label label-success\"> Revenue: </span>\r\n                    <h3>$1,234,112.20</h3>\r\n                  </div>\r\n                  <div class=\"col-md-3 col-sm-3 col-xs-6 text-stat\">\r\n                    <span class=\"label label-info\"> Tax: </span>\r\n                    <h3>$134,90.10</h3>\r\n                  </div>\r\n                  <div class=\"col-md-3 col-sm-3 col-xs-6 text-stat\">\r\n                    <span class=\"label label-danger\"> Shipment: </span>\r\n                    <h3>$1,134,90.10</h3>\r\n                  </div>\r\n                  <div class=\"col-md-3 col-sm-3 col-xs-6 text-stat\">\r\n                    <span class=\"label label-warning\"> Orders: </span>\r\n                    <h3>235090</h3>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <!-- End: life time stats -->\r\n        </div>\r\n      </div>\r\n      <div class=\"row\">\r\n        <div class=\"col-md-6 col-sm-6\">\r\n          <!-- BEGIN PORTLET-->\r\n          <div class=\"portlet light bordered\">\r\n            <div class=\"portlet-title tabbable-line\">\r\n              <div class=\"caption\">\r\n                <i class=\"icon-globe font-dark hide\"></i>\r\n                <span class=\"caption-subject font-dark bold uppercase\">Feeds</span>\r\n              </div>\r\n              <ul class=\"nav nav-tabs\">\r\n                <li class=\"active\">\r\n                  <a href=\"#tab_1_1\" class=\"active\" data-toggle=\"tab\"> System </a>\r\n                </li>\r\n                <li>\r\n                  <a href=\"#tab_1_2\" data-toggle=\"tab\"> Activities </a>\r\n                </li>\r\n              </ul>\r\n            </div>\r\n            <div class=\"portlet-body\">\r\n              <!--BEGIN TABS-->\r\n              <div class=\"tab-content\">\r\n                <div class=\"tab-pane active\" id=\"tab_1_1\">\r\n                  <div class=\"scroller\" style=\"height: 339px;\" data-always-visible=\"1\" data-rail-visible=\"0\">\r\n                    <ul class=\"feeds\">\r\n                      <li>\r\n                        <div class=\"col1\">\r\n                          <div class=\"cont\">\r\n                            <div class=\"cont-col1\">\r\n                              <div class=\"label label-sm label-success\">\r\n                                <i class=\"fa fa-bell-o\"></i>\r\n                              </div>\r\n                            </div>\r\n                            <div class=\"cont-col2\">\r\n                              <div class=\"desc\">\r\n                                You have 4 pending tasks.\r\n                                <span class=\"label label-sm label-info\">\r\n                                  Take action\r\n                                  <i class=\"fa fa-share\"></i>\r\n                                </span>\r\n                              </div>\r\n                            </div>\r\n                          </div>\r\n                        </div>\r\n                        <div class=\"col2\">\r\n                          <div class=\"date\"> Just now </div>\r\n                        </div>\r\n                      </li>\r\n                      <li>\r\n                        <a href=\"javascript:;\">\r\n                          <div class=\"col1\">\r\n                            <div class=\"cont\">\r\n                              <div class=\"cont-col1\">\r\n                                <div class=\"label label-sm label-success\">\r\n                                  <i class=\"fa fa-bell-o\"></i>\r\n                                </div>\r\n                              </div>\r\n                              <div class=\"cont-col2\">\r\n                                <div class=\"desc\"> New version v1.4 just lunched! </div>\r\n                              </div>\r\n                            </div>\r\n                          </div>\r\n                          <div class=\"col2\">\r\n                            <div class=\"date\"> 20 mins </div>\r\n                          </div>\r\n                        </a>\r\n                      </li>\r\n                      <li>\r\n                        <div class=\"col1\">\r\n                          <div class=\"cont\">\r\n                            <div class=\"cont-col1\">\r\n                              <div class=\"label label-sm label-danger\">\r\n                                <i class=\"fa fa-bolt\"></i>\r\n                              </div>\r\n                            </div>\r\n                            <div class=\"cont-col2\">\r\n                              <div class=\"desc\"> Database server #12 overloaded. Please fix the issue. </div>\r\n                            </div>\r\n                          </div>\r\n                        </div>\r\n                        <div class=\"col2\">\r\n                          <div class=\"date\"> 24 mins </div>\r\n                        </div>\r\n                      </li>\r\n                      <li>\r\n                        <div class=\"col1\">\r\n                          <div class=\"cont\">\r\n                            <div class=\"cont-col1\">\r\n                              <div class=\"label label-sm label-info\">\r\n                                <i class=\"fa fa-bullhorn\"></i>\r\n                              </div>\r\n                            </div>\r\n                            <div class=\"cont-col2\">\r\n                              <div class=\"desc\"> New order received. Please take care of it. </div>\r\n                            </div>\r\n                          </div>\r\n                        </div>\r\n                        <div class=\"col2\">\r\n                          <div class=\"date\"> 30 mins </div>\r\n                        </div>\r\n                      </li>\r\n                      <li>\r\n                        <div class=\"col1\">\r\n                          <div class=\"cont\">\r\n                            <div class=\"cont-col1\">\r\n                              <div class=\"label label-sm label-success\">\r\n                                <i class=\"fa fa-bullhorn\"></i>\r\n                              </div>\r\n                            </div>\r\n                            <div class=\"cont-col2\">\r\n                              <div class=\"desc\"> New order received. Please take care of it. </div>\r\n                            </div>\r\n                          </div>\r\n                        </div>\r\n                        <div class=\"col2\">\r\n                          <div class=\"date\"> 40 mins </div>\r\n                        </div>\r\n                      </li>\r\n                      <li>\r\n                        <div class=\"col1\">\r\n                          <div class=\"cont\">\r\n                            <div class=\"cont-col1\">\r\n                              <div class=\"label label-sm label-warning\">\r\n                                <i class=\"fa fa-plus\"></i>\r\n                              </div>\r\n                            </div>\r\n                            <div class=\"cont-col2\">\r\n                              <div class=\"desc\"> New user registered. </div>\r\n                            </div>\r\n                          </div>\r\n                        </div>\r\n                        <div class=\"col2\">\r\n                          <div class=\"date\"> 1.5 hours </div>\r\n                        </div>\r\n                      </li>\r\n                      <li>\r\n                        <div class=\"col1\">\r\n                          <div class=\"cont\">\r\n                            <div class=\"cont-col1\">\r\n                              <div class=\"label label-sm label-success\">\r\n                                <i class=\"fa fa-bell-o\"></i>\r\n                              </div>\r\n                            </div>\r\n                            <div class=\"cont-col2\">\r\n                              <div class=\"desc\">\r\n                                Web server hardware needs to be upgraded.\r\n                                <span class=\"label label-sm label-default \"> Overdue </span>\r\n                              </div>\r\n                            </div>\r\n                          </div>\r\n                        </div>\r\n                        <div class=\"col2\">\r\n                          <div class=\"date\"> 2 hours </div>\r\n                        </div>\r\n                      </li>\r\n                      <li>\r\n                        <div class=\"col1\">\r\n                          <div class=\"cont\">\r\n                            <div class=\"cont-col1\">\r\n                              <div class=\"label label-sm label-default\">\r\n                                <i class=\"fa fa-bullhorn\"></i>\r\n                              </div>\r\n                            </div>\r\n                            <div class=\"cont-col2\">\r\n                              <div class=\"desc\"> New order received. Please take care of it. </div>\r\n                            </div>\r\n                          </div>\r\n                        </div>\r\n                        <div class=\"col2\">\r\n                          <div class=\"date\"> 3 hours </div>\r\n                        </div>\r\n                      </li>\r\n                      <li>\r\n                        <div class=\"col1\">\r\n                          <div class=\"cont\">\r\n                            <div class=\"cont-col1\">\r\n                              <div class=\"label label-sm label-warning\">\r\n                                <i class=\"fa fa-bullhorn\"></i>\r\n                              </div>\r\n                            </div>\r\n                            <div class=\"cont-col2\">\r\n                              <div class=\"desc\"> New order received. Please take care of it. </div>\r\n                            </div>\r\n                          </div>\r\n                        </div>\r\n                        <div class=\"col2\">\r\n                          <div class=\"date\"> 5 hours </div>\r\n                        </div>\r\n                      </li>\r\n                      <li>\r\n                        <div class=\"col1\">\r\n                          <div class=\"cont\">\r\n                            <div class=\"cont-col1\">\r\n                              <div class=\"label label-sm label-info\">\r\n                                <i class=\"fa fa-bullhorn\"></i>\r\n                              </div>\r\n                            </div>\r\n                            <div class=\"cont-col2\">\r\n                              <div class=\"desc\"> New order received. Please take care of it. </div>\r\n                            </div>\r\n                          </div>\r\n                        </div>\r\n                        <div class=\"col2\">\r\n                          <div class=\"date\"> 18 hours </div>\r\n                        </div>\r\n                      </li>\r\n                      <li>\r\n                        <div class=\"col1\">\r\n                          <div class=\"cont\">\r\n                            <div class=\"cont-col1\">\r\n                              <div class=\"label label-sm label-default\">\r\n                                <i class=\"fa fa-bullhorn\"></i>\r\n                              </div>\r\n                            </div>\r\n                            <div class=\"cont-col2\">\r\n                              <div class=\"desc\"> New order received. Please take care of it. </div>\r\n                            </div>\r\n                          </div>\r\n                        </div>\r\n                        <div class=\"col2\">\r\n                          <div class=\"date\"> 21 hours </div>\r\n                        </div>\r\n                      </li>\r\n                      <li>\r\n                        <div class=\"col1\">\r\n                          <div class=\"cont\">\r\n                            <div class=\"cont-col1\">\r\n                              <div class=\"label label-sm label-info\">\r\n                                <i class=\"fa fa-bullhorn\"></i>\r\n                              </div>\r\n                            </div>\r\n                            <div class=\"cont-col2\">\r\n                              <div class=\"desc\"> New order received. Please take care of it. </div>\r\n                            </div>\r\n                          </div>\r\n                        </div>\r\n                        <div class=\"col2\">\r\n                          <div class=\"date\"> 22 hours </div>\r\n                        </div>\r\n                      </li>\r\n                      <li>\r\n                        <div class=\"col1\">\r\n                          <div class=\"cont\">\r\n                            <div class=\"cont-col1\">\r\n                              <div class=\"label label-sm label-default\">\r\n                                <i class=\"fa fa-bullhorn\"></i>\r\n                              </div>\r\n                            </div>\r\n                            <div class=\"cont-col2\">\r\n                              <div class=\"desc\"> New order received. Please take care of it. </div>\r\n                            </div>\r\n                          </div>\r\n                        </div>\r\n                        <div class=\"col2\">\r\n                          <div class=\"date\"> 21 hours </div>\r\n                        </div>\r\n                      </li>\r\n                      <li>\r\n                        <div class=\"col1\">\r\n                          <div class=\"cont\">\r\n                            <div class=\"cont-col1\">\r\n                              <div class=\"label label-sm label-info\">\r\n                                <i class=\"fa fa-bullhorn\"></i>\r\n                              </div>\r\n                            </div>\r\n                            <div class=\"cont-col2\">\r\n                              <div class=\"desc\"> New order received. Please take care of it. </div>\r\n                            </div>\r\n                          </div>\r\n                        </div>\r\n                        <div class=\"col2\">\r\n                          <div class=\"date\"> 22 hours </div>\r\n                        </div>\r\n                      </li>\r\n                      <li>\r\n                        <div class=\"col1\">\r\n                          <div class=\"cont\">\r\n                            <div class=\"cont-col1\">\r\n                              <div class=\"label label-sm label-default\">\r\n                                <i class=\"fa fa-bullhorn\"></i>\r\n                              </div>\r\n                            </div>\r\n                            <div class=\"cont-col2\">\r\n                              <div class=\"desc\"> New order received. Please take care of it. </div>\r\n                            </div>\r\n                          </div>\r\n                        </div>\r\n                        <div class=\"col2\">\r\n                          <div class=\"date\"> 21 hours </div>\r\n                        </div>\r\n                      </li>\r\n                      <li>\r\n                        <div class=\"col1\">\r\n                          <div class=\"cont\">\r\n                            <div class=\"cont-col1\">\r\n                              <div class=\"label label-sm label-info\">\r\n                                <i class=\"fa fa-bullhorn\"></i>\r\n                              </div>\r\n                            </div>\r\n                            <div class=\"cont-col2\">\r\n                              <div class=\"desc\"> New order received. Please take care of it. </div>\r\n                            </div>\r\n                          </div>\r\n                        </div>\r\n                        <div class=\"col2\">\r\n                          <div class=\"date\"> 22 hours </div>\r\n                        </div>\r\n                      </li>\r\n                      <li>\r\n                        <div class=\"col1\">\r\n                          <div class=\"cont\">\r\n                            <div class=\"cont-col1\">\r\n                              <div class=\"label label-sm label-default\">\r\n                                <i class=\"fa fa-bullhorn\"></i>\r\n                              </div>\r\n                            </div>\r\n                            <div class=\"cont-col2\">\r\n                              <div class=\"desc\"> New order received. Please take care of it. </div>\r\n                            </div>\r\n                          </div>\r\n                        </div>\r\n                        <div class=\"col2\">\r\n                          <div class=\"date\"> 21 hours </div>\r\n                        </div>\r\n                      </li>\r\n                      <li>\r\n                        <div class=\"col1\">\r\n                          <div class=\"cont\">\r\n                            <div class=\"cont-col1\">\r\n                              <div class=\"label label-sm label-info\">\r\n                                <i class=\"fa fa-bullhorn\"></i>\r\n                              </div>\r\n                            </div>\r\n                            <div class=\"cont-col2\">\r\n                              <div class=\"desc\"> New order received. Please take care of it. </div>\r\n                            </div>\r\n                          </div>\r\n                        </div>\r\n                        <div class=\"col2\">\r\n                          <div class=\"date\"> 22 hours </div>\r\n                        </div>\r\n                      </li>\r\n                    </ul>\r\n                  </div>\r\n                </div>\r\n                <div class=\"tab-pane\" id=\"tab_1_2\">\r\n                  <div class=\"scroller\" style=\"height: 290px;\" data-always-visible=\"1\" data-rail-visible1=\"1\">\r\n                    <ul class=\"feeds\">\r\n                      <li>\r\n                        <a href=\"javascript:;\">\r\n                          <div class=\"col1\">\r\n                            <div class=\"cont\">\r\n                              <div class=\"cont-col1\">\r\n                                <div class=\"label label-sm label-success\">\r\n                                  <i class=\"fa fa-bell-o\"></i>\r\n                                </div>\r\n                              </div>\r\n                              <div class=\"cont-col2\">\r\n                                <div class=\"desc\"> New user registered </div>\r\n                              </div>\r\n                            </div>\r\n                          </div>\r\n                          <div class=\"col2\">\r\n                            <div class=\"date\"> Just now </div>\r\n                          </div>\r\n                        </a>\r\n                      </li>\r\n                      <li>\r\n                        <a href=\"javascript:;\">\r\n                          <div class=\"col1\">\r\n                            <div class=\"cont\">\r\n                              <div class=\"cont-col1\">\r\n                                <div class=\"label label-sm label-success\">\r\n                                  <i class=\"fa fa-bell-o\"></i>\r\n                                </div>\r\n                              </div>\r\n                              <div class=\"cont-col2\">\r\n                                <div class=\"desc\"> New order received </div>\r\n                              </div>\r\n                            </div>\r\n                          </div>\r\n                          <div class=\"col2\">\r\n                            <div class=\"date\"> 10 mins </div>\r\n                          </div>\r\n                        </a>\r\n                      </li>\r\n                      <li>\r\n                        <div class=\"col1\">\r\n                          <div class=\"cont\">\r\n                            <div class=\"cont-col1\">\r\n                              <div class=\"label label-sm label-danger\">\r\n                                <i class=\"fa fa-bolt\"></i>\r\n                              </div>\r\n                            </div>\r\n                            <div class=\"cont-col2\">\r\n                              <div class=\"desc\">\r\n                                Order #24DOP4 has been rejected.\r\n                                <span class=\"label label-sm label-danger \">\r\n                                  Take action\r\n                                  <i class=\"fa fa-share\"></i>\r\n                                </span>\r\n                              </div>\r\n                            </div>\r\n                          </div>\r\n                        </div>\r\n                        <div class=\"col2\">\r\n                          <div class=\"date\"> 24 mins </div>\r\n                        </div>\r\n                      </li>\r\n                      <li>\r\n                        <a href=\"javascript:;\">\r\n                          <div class=\"col1\">\r\n                            <div class=\"cont\">\r\n                              <div class=\"cont-col1\">\r\n                                <div class=\"label label-sm label-success\">\r\n                                  <i class=\"fa fa-bell-o\"></i>\r\n                                </div>\r\n                              </div>\r\n                              <div class=\"cont-col2\">\r\n                                <div class=\"desc\"> New user registered </div>\r\n                              </div>\r\n                            </div>\r\n                          </div>\r\n                          <div class=\"col2\">\r\n                            <div class=\"date\"> Just now </div>\r\n                          </div>\r\n                        </a>\r\n                      </li>\r\n                      <li>\r\n                        <a href=\"javascript:;\">\r\n                          <div class=\"col1\">\r\n                            <div class=\"cont\">\r\n                              <div class=\"cont-col1\">\r\n                                <div class=\"label label-sm label-success\">\r\n                                  <i class=\"fa fa-bell-o\"></i>\r\n                                </div>\r\n                              </div>\r\n                              <div class=\"cont-col2\">\r\n                                <div class=\"desc\"> New user registered </div>\r\n                              </div>\r\n                            </div>\r\n                          </div>\r\n                          <div class=\"col2\">\r\n                            <div class=\"date\"> Just now </div>\r\n                          </div>\r\n                        </a>\r\n                      </li>\r\n                      <li>\r\n                        <a href=\"javascript:;\">\r\n                          <div class=\"col1\">\r\n                            <div class=\"cont\">\r\n                              <div class=\"cont-col1\">\r\n                                <div class=\"label label-sm label-success\">\r\n                                  <i class=\"fa fa-bell-o\"></i>\r\n                                </div>\r\n                              </div>\r\n                              <div class=\"cont-col2\">\r\n                                <div class=\"desc\"> New user registered </div>\r\n                              </div>\r\n                            </div>\r\n                          </div>\r\n                          <div class=\"col2\">\r\n                            <div class=\"date\"> Just now </div>\r\n                          </div>\r\n                        </a>\r\n                      </li>\r\n                      <li>\r\n                        <a href=\"javascript:;\">\r\n                          <div class=\"col1\">\r\n                            <div class=\"cont\">\r\n                              <div class=\"cont-col1\">\r\n                                <div class=\"label label-sm label-success\">\r\n                                  <i class=\"fa fa-bell-o\"></i>\r\n                                </div>\r\n                              </div>\r\n                              <div class=\"cont-col2\">\r\n                                <div class=\"desc\"> New user registered </div>\r\n                              </div>\r\n                            </div>\r\n                          </div>\r\n                          <div class=\"col2\">\r\n                            <div class=\"date\"> Just now </div>\r\n                          </div>\r\n                        </a>\r\n                      </li>\r\n                      <li>\r\n                        <a href=\"javascript:;\">\r\n                          <div class=\"col1\">\r\n                            <div class=\"cont\">\r\n                              <div class=\"cont-col1\">\r\n                                <div class=\"label label-sm label-success\">\r\n                                  <i class=\"fa fa-bell-o\"></i>\r\n                                </div>\r\n                              </div>\r\n                              <div class=\"cont-col2\">\r\n                                <div class=\"desc\"> New user registered </div>\r\n                              </div>\r\n                            </div>\r\n                          </div>\r\n                          <div class=\"col2\">\r\n                            <div class=\"date\"> Just now </div>\r\n                          </div>\r\n                        </a>\r\n                      </li>\r\n                      <li>\r\n                        <a href=\"javascript:;\">\r\n                          <div class=\"col1\">\r\n                            <div class=\"cont\">\r\n                              <div class=\"cont-col1\">\r\n                                <div class=\"label label-sm label-success\">\r\n                                  <i class=\"fa fa-bell-o\"></i>\r\n                                </div>\r\n                              </div>\r\n                              <div class=\"cont-col2\">\r\n                                <div class=\"desc\"> New user registered </div>\r\n                              </div>\r\n                            </div>\r\n                          </div>\r\n                          <div class=\"col2\">\r\n                            <div class=\"date\"> Just now </div>\r\n                          </div>\r\n                        </a>\r\n                      </li>\r\n                      <li>\r\n                        <a href=\"javascript:;\">\r\n                          <div class=\"col1\">\r\n                            <div class=\"cont\">\r\n                              <div class=\"cont-col1\">\r\n                                <div class=\"label label-sm label-success\">\r\n                                  <i class=\"fa fa-bell-o\"></i>\r\n                                </div>\r\n                              </div>\r\n                              <div class=\"cont-col2\">\r\n                                <div class=\"desc\"> New user registered </div>\r\n                              </div>\r\n                            </div>\r\n                          </div>\r\n                          <div class=\"col2\">\r\n                            <div class=\"date\"> Just now </div>\r\n                          </div>\r\n                        </a>\r\n                      </li>\r\n                    </ul>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <!--END TABS-->\r\n            </div>\r\n          </div>\r\n          <!-- END PORTLET-->\r\n        </div>\r\n        <div class=\"col-md-6 col-sm-6\">\r\n          <div class=\"portlet light bordered\">\r\n            <div class=\"portlet-title\">\r\n              <div class=\"caption caption-md\">\r\n                <i class=\"icon-bar-chart font-dark hide\"></i>\r\n                <span class=\"caption-subject font-dark bold uppercase\">Customer Support</span>\r\n                <span class=\"caption-helper\">45 pending</span>\r\n              </div>\r\n              <div class=\"inputs\">\r\n                <div class=\"portlet-input input-inline input-small \">\r\n                  <div class=\"input-icon right\">\r\n                    <i class=\"icon-magnifier\"></i>\r\n                    <input type=\"text\" class=\"form-control form-control-solid input-circle\" placeholder=\"search...\">\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n            <div class=\"portlet-body\">\r\n              <div class=\"scroller\" style=\"height: 338px;\" data-always-visible=\"1\" data-rail-visible1=\"0\" data-handle-color=\"#D7DCE2\">\r\n                <div class=\"general-item-list\">\r\n                  <div class=\"item\">\r\n                    <div class=\"item-head\">\r\n                      <div class=\"item-details\">\r\n                        <img class=\"item-pic rounded\" src=\"../assets/pages/media/users/avatar4.jpg\">\r\n                        <a href=\"\" class=\"item-name primary-link\">Nick Larson</a>\r\n                        <span class=\"item-label\">3 hrs ago</span>\r\n                      </div>\r\n                      <span class=\"item-status\">\r\n                        <span class=\"badge badge-empty badge-success\"></span> Open\r\n                      </span>\r\n                    </div>\r\n                    <div class=\"item-body\"> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </div>\r\n                  </div>\r\n                  <div class=\"item\">\r\n                    <div class=\"item-head\">\r\n                      <div class=\"item-details\">\r\n                        <img class=\"item-pic rounded\" src=\"../assets/pages/media/users/avatar3.jpg\">\r\n                        <a href=\"\" class=\"item-name primary-link\">Mark</a>\r\n                        <span class=\"item-label\">5 hrs ago</span>\r\n                      </div>\r\n                      <span class=\"item-status\">\r\n                        <span class=\"badge badge-empty badge-warning\"></span> Pending\r\n                      </span>\r\n                    </div>\r\n                    <div class=\"item-body\"> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat tincidunt ut laoreet. </div>\r\n                  </div>\r\n                  <div class=\"item\">\r\n                    <div class=\"item-head\">\r\n                      <div class=\"item-details\">\r\n                        <img class=\"item-pic rounded\" src=\"../assets/pages/media/users/avatar6.jpg\">\r\n                        <a href=\"\" class=\"item-name primary-link\">Nick Larson</a>\r\n                        <span class=\"item-label\">8 hrs ago</span>\r\n                      </div>\r\n                      <span class=\"item-status\">\r\n                        <span class=\"badge badge-empty badge-primary\"></span> Closed\r\n                      </span>\r\n                    </div>\r\n                    <div class=\"item-body\"> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh. </div>\r\n                  </div>\r\n                  <div class=\"item\">\r\n                    <div class=\"item-head\">\r\n                      <div class=\"item-details\">\r\n                        <img class=\"item-pic rounded\" src=\"../assets/pages/media/users/avatar7.jpg\">\r\n                        <a href=\"\" class=\"item-name primary-link\">Nick Larson</a>\r\n                        <span class=\"item-label\">12 hrs ago</span>\r\n                      </div>\r\n                      <span class=\"item-status\">\r\n                        <span class=\"badge badge-empty badge-danger\"></span> Pending\r\n                      </span>\r\n                    </div>\r\n                    <div class=\"item-body\"> Consectetuer adipiscing elit Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </div>\r\n                  </div>\r\n                  <div class=\"item\">\r\n                    <div class=\"item-head\">\r\n                      <div class=\"item-details\">\r\n                        <img class=\"item-pic rounded\" src=\"../assets/pages/media/users/avatar9.jpg\">\r\n                        <a href=\"\" class=\"item-name primary-link\">Richard Stone</a>\r\n                        <span class=\"item-label\">2 days ago</span>\r\n                      </div>\r\n                      <span class=\"item-status\">\r\n                        <span class=\"badge badge-empty badge-danger\"></span> Open\r\n                      </span>\r\n                    </div>\r\n                    <div class=\"item-body\"> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, ut laoreet dolore magna aliquam erat volutpat. </div>\r\n                  </div>\r\n                  <div class=\"item\">\r\n                    <div class=\"item-head\">\r\n                      <div class=\"item-details\">\r\n                        <img class=\"item-pic rounded\" src=\"../assets/pages/media/users/avatar8.jpg\">\r\n                        <a href=\"\" class=\"item-name primary-link\">Dan</a>\r\n                        <span class=\"item-label\">3 days ago</span>\r\n                      </div>\r\n                      <span class=\"item-status\">\r\n                        <span class=\"badge badge-empty badge-warning\"></span> Pending\r\n                      </span>\r\n                    </div>\r\n                    <div class=\"item-body\"> Lorem ipsum dolor sit amet, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </div>\r\n                  </div>\r\n                  <div class=\"item\">\r\n                    <div class=\"item-head\">\r\n                      <div class=\"item-details\">\r\n                        <img class=\"item-pic rounded\" src=\"../assets/pages/media/users/avatar2.jpg\">\r\n                        <a href=\"\" class=\"item-name primary-link\">Larry</a>\r\n                        <span class=\"item-label\">4 hrs ago</span>\r\n                      </div>\r\n                      <span class=\"item-status\">\r\n                        <span class=\"badge badge-empty badge-success\"></span> Open\r\n                      </span>\r\n                    </div>\r\n                    <div class=\"item-body\"> Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n      <!-- END PAGE BASE CONTENT -->\r\n    \r\n"

/***/ }),

/***/ 140:
/***/ (function(module, exports) {

module.exports = "<table *ngIf=\"tableMode; else create\" class=\"table table-sm table-striped\">\r\n  <tr>\r\n    <th>ID</th>\r\n    <th>Name</th>\r\n    <th>Category</th>\r\n    <th>Supplier</th>\r\n    <th>Price</th>\r\n    <th></th>\r\n  </tr>\r\n  <tr *ngFor=\"let p of products\">\r\n    <ng-template [ngIf]=\"product?.productId != p.productId\" [ngIfElse]=\"edit\">\r\n      <td>{{p.productId}}</td>\r\n      <td>{{p.name}}</td>\r\n      <td>{{p.category}}</td>\r\n      <td>{{p.supplier?.name || '(None)'}}</td>\r\n      <td>{{p.price | currency:EUR:true}}</td>\r\n      <td>\r\n        <button class=\"btn btn-sm btn-primary\"\r\n                (click)=\"selectProduct(p.productId)\">\r\n          Edit\r\n        </button>\r\n        <button class=\"btn btn-sm btn-danger\"\r\n                (click)=\"deleteProduct(p.productId)\">\r\n          Delete\r\n        </button>\r\n      </td>\r\n    </ng-template>\r\n  </tr>\r\n  <tfoot>\r\n    <tr>\r\n      <td colspan=\"6\" class=\"text-center\">\r\n        <button class=\"btn btn-primary\"\r\n                (click)=\"clearProduct(); tableMode = false\">\r\n          Create\r\n        </button>\r\n      </td>\r\n    </tr>\r\n  </tfoot>\r\n</table>\r\n\r\n<ng-template #edit>\r\n  <td colspan=\"6\">\r\n    <admin-product-editor></admin-product-editor>\r\n    <div class=\"text-center\">\r\n      <button class=\"btn btn-sm btn-primary\" (click)=\"saveProduct()\">\r\n        Save\r\n      </button>\r\n      <button class=\"btn btn-sm btn-info\" (click)=\"clearProduct()\">\r\n        Cancel\r\n      </button>\r\n    </div>\r\n  </td>\r\n</ng-template>\r\n<ng-template #create>\r\n  <admin-product-editor></admin-product-editor>\r\n  <button class=\"btn btn-primary\" (click)=\"saveProduct()\">\r\n    Save\r\n  </button>\r\n  <button class=\"btn btn-info\" (click)=\"clearProduct()\">\r\n    Cancel\r\n  </button>\r\n</ng-template>\r\n"

/***/ }),

/***/ 141:
/***/ (function(module, exports) {

module.exports = "<div class=\"form-group\">\r\n  <label>Name</label>\r\n  <input class=\"form-control\" [(ngModel)]=\"product.name\" />\r\n</div>\r\n<div class=\"form-group\">\r\n  <label>Category</label>\r\n  <input class=\"form-control\" [(ngModel)]=\"product.category\" />\r\n</div>\r\n<div class=\"form-group\">\r\n  <label>Supplier</label>\r\n  <select class=\"form-control\" [(ngModel)]=\"product.supplier\"\r\n          [compareWith]=\"compareSuppliers\">\r\n    <option *ngFor=\"let s of suppliers\" [ngValue]=\"s\">{{s.name}}</option>\r\n  </select>\r\n</div>\r\n<div class=\"form-group\">\r\n  <label>Description</label>\r\n<textarea class=\"form-control\" [(ngModel)]=\"product.description\"></textarea>\r\n</div>\r\n<div class=\"form-group\">\r\n  <label>Price</label>\r\n  <input class=\"form-control\" [(ngModel)]=\"product.price\" />\r\n</div>\r\n\r\n<div class=\"row\">\r\n  <div class=\"col-md-12\">\r\n      <div class=\"portlet\">\r\n        <div class=\"portlet-title\">\r\n          <div class=\"caption\">\r\n            <i class=\"fa fa-shopping-cart\"></i>\r\n          </div>\r\n          <div class=\"actions btn-set\">\r\n            <button type=\"button\" name=\"back\" class=\"btn btn-secondary-outline\">\r\n              <i class=\"fa fa-angle-left\"></i> Back\r\n            </button>\r\n            <button class=\"btn btn-secondary-outline\">\r\n              <i class=\"fa fa-reply\"></i> Reset\r\n            </button>\r\n            <button class=\"btn btn-success\">\r\n              <i class=\"fa fa-check\"></i> Save\r\n            </button>\r\n            <button class=\"btn btn-success\">\r\n              <i class=\"fa fa-check-circle\"></i> Save & Continue Edit\r\n            </button>\r\n            <div class=\"btn-group\">\r\n              <a class=\"btn btn-success dropdown-toggle\" href=\"javascript:;\" data-toggle=\"dropdown\">\r\n                <i class=\"fa fa-share\"></i> More\r\n                <i class=\"fa fa-angle-down\"></i>\r\n              </a>\r\n              <div class=\"dropdown-menu pull-right\">\r\n                <li>\r\n                  <a href=\"javascript:;\"> Duplicate </a>\r\n                </li>\r\n                <li>\r\n                  <a href=\"javascript:;\"> Delete </a>\r\n                </li>\r\n                <li class=\"dropdown-divider\"> </li>\r\n                <li>\r\n                  <a href=\"javascript:;\"> Print </a>\r\n                </li>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n        <div class=\"portlet-body\">\r\n          <div class=\"tabbable-bordered\">\r\n            <ul class=\"nav nav-tabs\">\r\n              <li class=\"active\">\r\n                <a href=\"#tab_general\" data-toggle=\"tab\"> General </a>\r\n              </li>\r\n              <li>\r\n                <a href=\"#tab_meta\" data-toggle=\"tab\"> Meta </a>\r\n              </li>\r\n              <li>\r\n                <a href=\"#tab_images\" data-toggle=\"tab\"> Images </a>\r\n              </li>\r\n              <li>\r\n                <a href=\"#tab_reviews\" data-toggle=\"tab\">\r\n                  Reviews\r\n                  <span class=\"badge badge-success\"> 3 </span>\r\n                </a>\r\n              </li>\r\n              <li>\r\n                <a href=\"#tab_history\" data-toggle=\"tab\"> History </a>\r\n              </li>\r\n            </ul>\r\n            <div class=\"tab-content\">\r\n              <div class=\"tab-pane active\" id=\"tab_general\">\r\n                <div class=\"form-body\">\r\n                  <div class=\"form-group\">\r\n                    <label class=\"col-md-2 control-label\">\r\n                      Name:\r\n                      <span class=\"required\"> * </span>\r\n                    </label>\r\n                    <div class=\"col-md-10\">\r\n                      <input type=\"text\" class=\"form-control\" placeholder=\"\" [(ngModel)]=\"product.name\">\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"form-group\">\r\n                    <label class=\"col-md-2 control-label\">\r\n                      Description:\r\n                      <span class=\"required\"> * </span>\r\n                    </label>\r\n                    <div class=\"col-md-10\">\r\n                      <textarea class=\"form-control\" [(ngModel)]=\"product.description\"></textarea>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"form-group\">\r\n                    <label class=\"col-md-2 control-label\">\r\n                      Short Description:\r\n                      <span class=\"required\"> * </span>\r\n                    </label>\r\n                    <div class=\"col-md-10\">\r\n                      <textarea class=\"form-control\" name=\"product[short_description]\" [(ngModel)]=\"product.short_description\"></textarea>\r\n                      <span class=\"help-block\"> shown in product listing </span>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"form-group\">\r\n                    <label class=\"col-md-2 control-label\">\r\n                      Categories:\r\n                      <span class=\"required\"> * </span>\r\n                    </label>\r\n                    <div class=\"col-md-10\">\r\n                      <div class=\"form-control height-auto\">\r\n                        <div class=\"scroller\" style=\"height:275px;\" data-always-visible=\"1\">\r\n                          <ul class=\"list-unstyled\">\r\n                            <li>\r\n                              <label>\r\n                                <input type=\"checkbox\" name=\"product[categories][]\" value=\"1\">Mens\r\n                              </label>\r\n                              <ul class=\"list-unstyled\">\r\n                                <li>\r\n                                  <label>\r\n                                    <input type=\"checkbox\" name=\"product[categories][]\" value=\"1\">Footwear\r\n                                  </label>\r\n                                </li>\r\n                                <li>\r\n                                  <label>\r\n                                    <input type=\"checkbox\" name=\"product[categories][]\" value=\"1\">Clothing\r\n                                  </label>\r\n                                </li>\r\n                                <li>\r\n                                  <label>\r\n                                    <input type=\"checkbox\" name=\"product[categories][]\" value=\"1\">Accessories\r\n                                  </label>\r\n                                </li>\r\n                                <li>\r\n                                  <label>\r\n                                    <input type=\"checkbox\" name=\"product[categories][]\" value=\"1\">Fashion Outlet\r\n                                  </label>\r\n                                </li>\r\n                              </ul>\r\n                            </li>\r\n                            <li>\r\n                              <label>\r\n                                <input type=\"checkbox\" name=\"product[categories][]\" value=\"1\">Football Shirts\r\n                              </label>\r\n                              <ul class=\"list-unstyled\">\r\n                                <li>\r\n                                  <label>\r\n                                    <input type=\"checkbox\" name=\"product[categories][]\" value=\"1\">Premier League\r\n                                  </label>\r\n                                </li>\r\n                                <li>\r\n                                  <label>\r\n                                    <input type=\"checkbox\" name=\"product[categories][]\" value=\"1\">Football League\r\n                                  </label>\r\n                                </li>\r\n                                <li>\r\n                                  <label>\r\n                                    <input type=\"checkbox\" name=\"product[categories][]\" value=\"1\">Serie A\r\n                                  </label>\r\n                                </li>\r\n                                <li>\r\n                                  <label>\r\n                                    <input type=\"checkbox\" name=\"product[categories][]\" value=\"1\">Bundesliga\r\n                                  </label>\r\n                                </li>\r\n                              </ul>\r\n                            </li>\r\n                            <li>\r\n                              <label>\r\n                                <input type=\"checkbox\" name=\"product[categories][]\" value=\"1\">Brands\r\n                              </label>\r\n                              <ul class=\"list-unstyled\">\r\n                                <li>\r\n                                  <label>\r\n                                    <input type=\"checkbox\" name=\"product[categories][]\" value=\"1\">Adidas\r\n                                  </label>\r\n                                </li>\r\n                                <li>\r\n                                  <label>\r\n                                    <input type=\"checkbox\" name=\"product[categories][]\" value=\"1\">Nike\r\n                                  </label>\r\n                                </li>\r\n                                <li>\r\n                                  <label>\r\n                                    <input type=\"checkbox\" name=\"product[categories][]\" value=\"1\">Airwalk\r\n                                  </label>\r\n                                </li>\r\n                                <li>\r\n                                  <label>\r\n                                    <input type=\"checkbox\" name=\"product[categories][]\" value=\"1\">Kangol\r\n                                  </label>\r\n                                </li>\r\n                              </ul>\r\n                            </li>\r\n                          </ul>\r\n                        </div>\r\n                      </div>\r\n                      <span class=\"help-block\"> select one or more categories </span>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"form-group\">\r\n                    <label class=\"col-md-2 control-label\">\r\n                      Available Date:\r\n                      <span class=\"required\"> * </span>\r\n                    </label>\r\n                    <div class=\"col-md-10\">\r\n                      <div class=\"input-group input-large date-picker input-daterange\" data-date=\"10/11/2012\" data-date-format=\"mm/dd/yyyy\">\r\n                        <input type=\"text\" class=\"form-control\" name=\"product[available_from]\" [(ngModel)]=\"product.available_from\">\r\n                        <span class=\"input-group-addon\"> to </span>\r\n                        <input type=\"text\" class=\"form-control\" name=\"product[available_to]\" [(ngModel)]=\"product.available_to\">\r\n                      </div>\r\n                      <span class=\"help-block\"> availability daterange. </span>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"form-group\">\r\n                    <label class=\"col-md-2 control-label\">\r\n                      Price:\r\n                      <span class=\"required\"> * </span>\r\n                    </label>\r\n                    <div class=\"col-md-10\">\r\n                      <input type=\"text\" class=\"form-control\" name=\"product[price]\" placeholder=\"\" [(ngModel)]=\"product.price\">\r\n                    </div>\r\n                  </div>\r\n                  \r\n                  <div class=\"form-group\">\r\n                    <label class=\"col-md-2 control-label\">\r\n                      Status:\r\n                      <span class=\"required\"> * </span>\r\n                    </label>\r\n                    <div class=\"col-md-10\">\r\n                      <select class=\"table-group-action-input form-control input-medium\" name=\"product[status]\" [(ngModel)]=\"product.price\">\r\n                        <option value=\"\">Select...</option>\r\n                        <option value=\"1\">Published</option>\r\n                        <option value=\"0\">Not Published</option>\r\n                      </select>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div class=\"tab-pane\" id=\"tab_meta\">\r\n                <div class=\"form-body\">\r\n                  <div class=\"form-group\">\r\n                    <label class=\"col-md-2 control-label\">Meta Title:</label>\r\n                    <div class=\"col-md-10\">\r\n                      <input type=\"text\" class=\"form-control maxlength-handler\" name=\"product[meta_title]\" maxlength=\"100\" placeholder=\"\" [(ngModel)]=\"product.meta_title\" />\r\n                      <span class=\"help-block\"> max 100 chars </span>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"form-group\">\r\n                    <label class=\"col-md-2 control-label\">Meta Keywords:</label>\r\n                    <div class=\"col-md-10\">\r\n                      <textarea class=\"form-control maxlength-handler\" rows=\"8\" name=\"product[meta_keywords]\" [(ngModel)]=\"product.meta_keywords\" maxlength=\"1000\"></textarea>\r\n                      <span class=\"help-block\"> max 1000 chars </span>\r\n                    </div>\r\n                  </div>\r\n                  <div class=\"form-group\">\r\n                    <label class=\"col-md-2 control-label\">Meta Description:</label>\r\n                    <div class=\"col-md-10\">\r\n                      <textarea class=\"form-control maxlength-handler\" rows=\"8\" name=\"product[meta_description]\" maxlength=\"255\" [(ngModel)]=\"product.meta_description\"></textarea>\r\n                      <span class=\"help-block\"> max 255 chars </span>\r\n                    </div>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n              <div class=\"tab-pane\" id=\"tab_images\">\r\n                <div class=\"alert alert-success margin-bottom-10\">\r\n                  <button type=\"button\" class=\"close\" data-dismiss=\"alert\" aria-hidden=\"true\"></button>\r\n                  <i class=\"fa fa-warning fa-lg\"></i> Image type and information need to be specified.\r\n                </div>\r\n                <div id=\"tab_images_uploader_container\" class=\"text-align-reverse margin-bottom-10\">\r\n                  <a id=\"tab_images_uploader_pickfiles\" href=\"javascript:;\" class=\"btn btn-success\">\r\n                    <i class=\"fa fa-plus\"></i> Select Files\r\n                  </a>\r\n                  <a id=\"tab_images_uploader_uploadfiles\" href=\"javascript:;\" class=\"btn btn-primary\">\r\n                    <i class=\"fa fa-share\"></i> Upload Files\r\n                  </a>\r\n                </div>\r\n                <div class=\"row\">\r\n                  <div id=\"tab_images_uploader_filelist\" class=\"col-md-6 col-sm-12\"> </div>\r\n                </div>\r\n                <table class=\"table table-bordered table-hover\">\r\n                  <thead>\r\n                    <tr role=\"row\" class=\"heading\">\r\n                      <th width=\"8%\"> Image </th>\r\n                      <th width=\"25%\"> Label </th>\r\n                      <th width=\"8%\"> Sort Order </th>\r\n                      <th width=\"10%\"> Base Image </th>\r\n                      <th width=\"10%\"> Small Image </th>\r\n                      <th width=\"10%\"> Thumbnail </th>\r\n                      <th width=\"10%\"> </th>\r\n                    </tr>\r\n                  </thead>\r\n                  <tbody>\r\n                    <tr>\r\n                      <td>\r\n                        <a href=\"../assets/pages/media/works/img2.jpg\" class=\"fancybox-button\" data-rel=\"fancybox-button\">\r\n                          <img class=\"img-responsive\" src=\"../assets/pages/media/works/img2.jpg\" alt=\"\">\r\n                        </a>\r\n                      </td>\r\n                      <td>\r\n                        <input type=\"text\" class=\"form-control\" name=\"product[images][2][label]\" value=\"Product image #1\">\r\n                      </td>\r\n                      <td>\r\n                        <input type=\"text\" class=\"form-control\" name=\"product[images][2][sort_order]\" value=\"1\">\r\n                      </td>\r\n                      <td>\r\n                        <label>\r\n                          <input type=\"radio\" name=\"product[images][2][image_type]\" value=\"1\">\r\n                        </label>\r\n                      </td>\r\n                      <td>\r\n                        <label>\r\n                          <input type=\"radio\" name=\"product[images][2][image_type]\" value=\"2\" checked>\r\n                        </label>\r\n                      </td>\r\n                      <td>\r\n                        <label>\r\n                          <input type=\"radio\" name=\"product[images][2][image_type]\" value=\"3\">\r\n                        </label>\r\n                      </td>\r\n                      <td>\r\n                        <a href=\"javascript:;\" class=\"btn btn-default btn-sm\">\r\n                          <i class=\"fa fa-times\"></i> Remove\r\n                        </a>\r\n                      </td>\r\n                    </tr>\r\n                    <tr>\r\n                      <td>\r\n                        <a href=\"../assets/pages/media/works/img3.jpg\" class=\"fancybox-button\" data-rel=\"fancybox-button\">\r\n                          <img class=\"img-responsive\" src=\"../assets/pages/media/works/img3.jpg\" alt=\"\">\r\n                        </a>\r\n                      </td>\r\n                      <td>\r\n                        <input type=\"text\" class=\"form-control\" name=\"product[images][3][label]\" value=\"Product image #2\">\r\n                      </td>\r\n                      <td>\r\n                        <input type=\"text\" class=\"form-control\" name=\"product[images][3][sort_order]\" value=\"1\">\r\n                      </td>\r\n                      <td>\r\n                        <label>\r\n                          <input type=\"radio\" name=\"product[images][3][image_type]\" value=\"1\" checked>\r\n                        </label>\r\n                      </td>\r\n                      <td>\r\n                        <label>\r\n                          <input type=\"radio\" name=\"product[images][3][image_type]\" value=\"2\">\r\n                        </label>\r\n                      </td>\r\n                      <td>\r\n                        <label>\r\n                          <input type=\"radio\" name=\"product[images][3][image_type]\" value=\"3\">\r\n                        </label>\r\n                      </td>\r\n                      <td>\r\n                        <a href=\"javascript:;\" class=\"btn btn-default btn-sm\">\r\n                          <i class=\"fa fa-times\"></i> Remove\r\n                        </a>\r\n                      </td>\r\n                    </tr>\r\n                  </tbody>\r\n                </table>\r\n              </div>\r\n              <div class=\"tab-pane\" id=\"tab_reviews\">\r\n                <div class=\"table-container\">\r\n                  <table class=\"table table-striped table-bordered table-hover\" id=\"datatable_reviews\">\r\n                    <thead>\r\n                      <tr role=\"row\" class=\"heading\">\r\n                        <th width=\"5%\"> Review&nbsp;# </th>\r\n                        <th width=\"10%\"> Review&nbsp;Date </th>\r\n                        <th width=\"10%\"> Customer </th>\r\n                        <th width=\"20%\"> Review&nbsp;Content </th>\r\n                        <th width=\"10%\"> Status </th>\r\n                        <th width=\"10%\"> Actions </th>\r\n                      </tr>\r\n                      <tr role=\"row\" class=\"filter\">\r\n                        <td>\r\n                          <input type=\"text\" class=\"form-control form-filter input-sm\" name=\"product_review_no\">\r\n                        </td>\r\n                        <td>\r\n                          <div class=\"input-group date date-picker margin-bottom-5\" data-date-format=\"dd/mm/yyyy\">\r\n                            <input type=\"text\" class=\"form-control form-filter input-sm\" readonly name=\"product_review_date_from\" placeholder=\"From\">\r\n                            <span class=\"input-group-btn\">\r\n                              <button class=\"btn btn-sm default\" type=\"button\">\r\n                                <i class=\"fa fa-calendar\"></i>\r\n                              </button>\r\n                            </span>\r\n                          </div>\r\n                          <div class=\"input-group date date-picker\" data-date-format=\"dd/mm/yyyy\">\r\n                            <input type=\"text\" class=\"form-control form-filter input-sm\" readonly name=\"product_review_date_to\" placeholder=\"To\">\r\n                            <span class=\"input-group-btn\">\r\n                              <button class=\"btn btn-sm default\" type=\"button\">\r\n                                <i class=\"fa fa-calendar\"></i>\r\n                              </button>\r\n                            </span>\r\n                          </div>\r\n                        </td>\r\n                        <td>\r\n                          <input type=\"text\" class=\"form-control form-filter input-sm\" name=\"product_review_customer\">\r\n                        </td>\r\n                        <td>\r\n                          <input type=\"text\" class=\"form-control form-filter input-sm\" name=\"product_review_content\">\r\n                        </td>\r\n                        <td>\r\n                          <select name=\"product_review_status\" class=\"form-control form-filter input-sm\">\r\n                            <option value=\"\">Select...</option>\r\n                            <option value=\"pending\">Pending</option>\r\n                            <option value=\"approved\">Approved</option>\r\n                            <option value=\"rejected\">Rejected</option>\r\n                          </select>\r\n                        </td>\r\n                        <td>\r\n                          <div class=\"margin-bottom-5\">\r\n                            <button class=\"btn btn-sm btn-success filter-submit margin-bottom\">\r\n                              <i class=\"fa fa-search\"></i> Search\r\n                            </button>\r\n                          </div>\r\n                          <button class=\"btn btn-sm btn-danger filter-cancel\">\r\n                            <i class=\"fa fa-times\"></i> Reset\r\n                          </button>\r\n                        </td>\r\n                      </tr>\r\n                    </thead>\r\n                    <tbody> </tbody>\r\n                  </table>\r\n                </div>\r\n              </div>\r\n              <div class=\"tab-pane\" id=\"tab_history\">\r\n                <div class=\"table-container\">\r\n                  <table class=\"table table-striped table-bordered table-hover\" id=\"datatable_history\">\r\n                    <thead>\r\n                      <tr role=\"row\" class=\"heading\">\r\n                        <th width=\"25%\"> Datetime </th>\r\n                        <th width=\"55%\"> Description </th>\r\n                        <th width=\"10%\"> Notification </th>\r\n                        <th width=\"10%\"> Actions </th>\r\n                      </tr>\r\n                      <tr role=\"row\" class=\"filter\">\r\n                        <td>\r\n                          <div class=\"input-group date datetime-picker margin-bottom-5\" data-date-format=\"dd/mm/yyyy hh:ii\">\r\n                            <input type=\"text\" class=\"form-control form-filter input-sm\" readonly name=\"product_history_date_from\" placeholder=\"From\">\r\n                            <span class=\"input-group-btn\">\r\n                              <button class=\"btn btn-sm default date-set\" type=\"button\">\r\n                                <i class=\"fa fa-calendar\"></i>\r\n                              </button>\r\n                            </span>\r\n                          </div>\r\n                          <div class=\"input-group date datetime-picker\" data-date-format=\"dd/mm/yyyy hh:ii\">\r\n                            <input type=\"text\" class=\"form-control form-filter input-sm\" readonly name=\"product_history_date_to\" placeholder=\"To\">\r\n                            <span class=\"input-group-btn\">\r\n                              <button class=\"btn btn-sm default date-set\" type=\"button\">\r\n                                <i class=\"fa fa-calendar\"></i>\r\n                              </button>\r\n                            </span>\r\n                          </div>\r\n                        </td>\r\n                        <td>\r\n                          <input type=\"text\" class=\"form-control form-filter input-sm\" name=\"product_history_desc\" placeholder=\"To\" />\r\n                        </td>\r\n                        <td>\r\n                          <select name=\"product_history_notification\" class=\"form-control form-filter input-sm\">\r\n                            <option value=\"\">Select...</option>\r\n                            <option value=\"pending\">Pending</option>\r\n                            <option value=\"notified\">Notified</option>\r\n                            <option value=\"failed\">Failed</option>\r\n                          </select>\r\n                        </td>\r\n                        <td>\r\n                          <div class=\"margin-bottom-5\">\r\n                            <button class=\"btn btn-sm btn-default filter-submit margin-bottom\">\r\n                              <i class=\"fa fa-search\"></i> Search\r\n                            </button>\r\n                          </div>\r\n                          <button class=\"btn btn-sm btn-danger-outline filter-cancel\">\r\n                            <i class=\"fa fa-times\"></i> Reset\r\n                          </button>\r\n                        </td>\r\n                      </tr>\r\n                    </thead>\r\n                    <tbody> </tbody>\r\n                  </table>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n  </div>\r\n</div>\r\n\r\n"

/***/ }),

/***/ 142:
/***/ (function(module, exports) {

module.exports = "<div class=\"bg-danger text-white text-center p-2 m-2\" *ngIf=\"error != null\">\r\n  <h6 *ngFor=\"let e of error\">{{e}}</h6>\r\n  <button class=\"btn btn-warning\" (click)=\"clearError()\">OK</button>\r\n</div>\r\n<router-outlet></router-outlet>\r\n"

/***/ }),

/***/ 143:
/***/ (function(module, exports) {

module.exports = "<div class=\"navbar bg-info mb-1\">\r\n  <a class=\"navbar-brand text-white\">Shop rova Admin</a>\r\n</div>\r\n<h4 *ngIf=\"showError\" class=\"p-2 bg-danger text-white\">\r\n  Invalid username or password\r\n</h4>\r\n<form novalidate #authForm=\"ngForm\">\r\n  <div class=\"form-group\">\r\n    <label>Name:</label>\r\n    <input #name=\"ngModel\" name=\"name\" class=\"form-control\"\r\n           [(ngModel)]=\"authService.name\" required />\r\n    <div *ngIf=\"name.invalid\" class=\"text-danger\">\r\n      Please enter your user name\r\n    </div>\r\n  </div>\r\n  <div class=\"form-group\">\r\n    <label>Password:</label>\r\n    <input type=\"password\" #password=\"ngModel\" name=\"password\"\r\n           class=\"form-control\" [(ngModel)]=\"authService.password\" required />\r\n    <div *ngIf=\"password.invalid\" class=\"text-danger\">\r\n      Please enter your password\r\n    </div>\r\n  </div>\r\n  <div class=\"text-center pt-2\">\r\n    <button class=\"btn btn-primary\" [disabled]=\"authForm.invalid\"\r\n            (click)=\"login()\">\r\n      Login\r\n    </button>\r\n  </div>\r\n</form>\r\n"

/***/ }),

/***/ 144:
/***/ (function(module, exports) {

module.exports = "<div class=\"navbar bg-inverse \">\r\n  <a class=\"navbar-brand text-white\">Shop rova</a>\r\n</div>\r\n<div class=\"m-1\">\r\n  <h2 class=\"text-center\">Votre panier</h2>\r\n  <table class=\"table table-bordered table-striped p-1\">\r\n    <thead>\r\n      <tr>\r\n        <th>Quantité</th>\r\n        <th>Produit</th>\r\n        <th class=\"text-right\">Prix</th>\r\n        <th class=\"text-right\">Sous-total</th>\r\n      </tr>\r\n    </thead>\r\n    <tbody>\r\n      <tr *ngIf=\"cart.selections.length == 0\">\r\n        <td colspan=\"4\" class=\"text-xs-center\">\r\n          Votre panier est vide\r\n        </td>\r\n      </tr>\r\n      <tr *ngFor=\"let sel of cart.selections\">\r\n        <td>\r\n          <input type=\"number\" class=\"form-control-sm\"\r\n                 style=\"width:5em\" [(ngModel)]=\"sel.quantity\" />\r\n        </td>\r\n        <td>{{sel.name}}</td>\r\n        <td class=\"text-right\">\r\n          {{sel.price | currency:\"EUR\":true:\"2.2-2\"}}\r\n        </td>\r\n        <td class=\"text-right\">\r\n          {{(sel.quantity * sel.price) | currency:\"EUR\":true:\"2.2-2\" }}\r\n        </td>\r\n        <td class=\"text-center\">\r\n          <button class=\"btn btn-sm btn-danger\"\r\n                  (click)=\"cart.updateQuantity(sel.productId, 0)\">\r\n            Remove\r\n          </button>\r\n        </td>\r\n      </tr>\r\n    </tbody>\r\n    <tfoot>\r\n      <tr>\r\n        <td colspan=\"3\" class=\"text-right\">Total:</td>\r\n        <td class=\"text-right\">\r\n          {{cart.totalPrice | currency:\"EUR\":true:\"2.2-2\"}}\r\n        </td>\r\n      </tr>\r\n    </tfoot>\r\n  </table>\r\n</div>\r\n<div class=\"text-center\">\r\n  <button class=\"btn btn-primary\" routerLink=\"/store\">Continue Shopping</button>\r\n  <button class=\"btn btn-secondary\" routerLink=\"/checkout\"\r\n          [disabled]=\"cart.selections.length == 0\">\r\n    Checkout\r\n  </button>\r\n</div>\r\n"

/***/ }),

/***/ 145:
/***/ (function(module, exports) {

module.exports = "<div class=\"text-right p-1\">\r\n  <small *ngIf=\"itemCount > 0; else empty\" class=\"text-primary\">\r\n    ({{ itemCount }} Produit(s) dans le panier {{ totalPrice | currency:\"EUR\":true }})\r\n  </small>\r\n  <button class=\"btn btn-sm ml-1 btn-primary\"\r\n          [disabled]=\"itemCount == 0\"\r\n          routerLink=\"/cart\">\r\n    Voir le panier\r\n    <i class=\"glyphicon glyphicon-shopping-cart\"> </i>\r\n  </button>\r\n</div>\r\n<ng-template #empty>\r\n  <small class=\"text-muted\">\r\n    (le panier est vide)\r\n  </small>\r\n</ng-template>\r\n"

/***/ }),

/***/ 146:
/***/ (function(module, exports) {

module.exports = "<div class=\"page-sidebar\">\r\n  <nav class=\"navbar\" role=\"navigation\">\r\n    <!-- Brand and toggle get grouped for better mobile display -->\r\n    <!-- Collect the nav links, forms, and other content for toggling -->\r\n    <ul class=\"nav navbar-nav margin-bottom-35\">\r\n      <li class=\"btn btn-outline-primary btn-block\" [class.active]=\"currentCategory == null\">\r\n        <a href=\"#\" (click)=\"setCurrentCategory(null)\">\r\n           Voir tout\r\n        </a>\r\n      </li>\r\n      <li *ngFor=\"let category of categories\" href=\"#\" class=\"btn btn-outline-primary btn-block\"\r\n     [class.active]=\"currentCategory == category\">\r\n        <a href=\"#\" (click)=\"setCurrentCategory(category)\">\r\n           {{ category }}\r\n        </a>\r\n      </li>\r\n    </ul>\r\n    \r\n  </nav>\r\n</div>\r\n"

/***/ }),

/***/ 147:
/***/ (function(module, exports) {

module.exports = "<div class=\"navbar bg-inverse \">\r\n  <a class=\"navbar-brand text-white\">Shop rova</a>\r\n</div>\r\n<h2 class=\"text-center mt-1\">Step 1: Vos informations</h2>\r\n<form novalidate #detailsForm=\"ngForm\">\r\n  <div class=\"form-group\">\r\n    <label>Nom</label>\r\n    <input #name=\"ngModel\" name=\"name\" class=\"form-control\"\r\n           [(ngModel)]=\"order.name\" required />\r\n    <div *ngIf=\"name.invalid\" class=\"text-danger\">\r\n      Renseignez votre nom\r\n    </div>\r\n  </div>\r\n  <div class=\"form-group\">\r\n    <label>Address</label>\r\n    <input #address=\"ngModel\" name=\"street\" class=\"form-control\"\r\n           [(ngModel)]=\"order.address\" required />\r\n    <div *ngIf=\"address.invalid\" class=\"text-danger\">\r\n      Renseignez votre address\r\n    </div>\r\n  </div>\r\n  <div class=\"text-center pt-2\">\r\n    <button class=\"btn btn-outline-primary\" routerLink=\"/cart\">Back</button>\r\n    <button class=\"btn btn-danger\" [disabled]=\"detailsForm.invalid\"\r\n            routerLink=\"/checkout/step2\">\r\n      Next\r\n    </button>\r\n  </div>\r\n</form>\r\n"

/***/ }),

/***/ 148:
/***/ (function(module, exports) {

module.exports = "<div class=\"navbar bg-inverse \">\r\n  <a class=\"navbar-brand text-white\">Shop rova</a>\r\n</div>\r\n<h2 class=\"text-center mt-1\">Step 2: Payment</h2>\r\n<form novalidate #paymentForm=\"ngForm\">\r\n  <div class=\"form-group\">\r\n    <label>Card Number</label>\r\n    <input #cardNumber=\"ngModel\" name=\"cardNumber\" class=\"form-control\"\r\n           [(ngModel)]=\"order.payment.cardNumber\" required />\r\n    <div *ngIf=\"cardNumber.invalid\" class=\"text-danger\">\r\n      Entrez vos information de carte\r\n    </div>\r\n  </div>\r\n  <div class=\"form-group\">\r\n    <label>Date d'expiration de la carte</label>\r\n    <input #cardExpiry=\"ngModel\" name=\"cardExpiry\" class=\"form-control\"\r\n           [(ngModel)]=\"order.payment.cardExpiry\" required />\r\n    <div *ngIf=\"cardExpiry.invalid\" class=\"text-danger\">\r\n      Entrez la date d'expiration'\r\n    </div>\r\n  </div>\r\n  <div class=\"form-group\">\r\n    <label>Security Code</label>\r\n    <input #cardCode=\"ngModel\" name=\"cardCode\" class=\"form-control\"\r\n           [(ngModel)]=\"order.payment.cardSecurityCode\" required />\r\n    <div *ngIf=\"cardCode.invalid\" class=\"text-danger\">\r\n      Entrez le code de sécurités\r\n    </div>\r\n  </div>\r\n  <div class=\"text-center pt-2\">\r\n    <button class=\"btn btn-outline-primary\" routerLink=\"/checkout/step1\">\r\n      Back\r\n    </button>\r\n    <button class=\"btn btn-danger\" [disabled]=\"paymentForm.invalid\"\r\n            routerLink=\"/checkout/step3\">\r\n      Next\r\n    </button>\r\n  </div>\r\n</form>\r\n"

/***/ }),

/***/ 149:
/***/ (function(module, exports) {

module.exports = "<h2 class=\"text-center\">Résumé</h2>\r\n<div class=\"container\">\r\n  <table class=\"table m-2\">\r\n    <tr><th>Nom</th><td>{{order.name}}</td></tr>\r\n    <tr><th>Address</th><td>{{order.address}}</td></tr>\r\n    <tr><th>Produit</th><td>{{order.cart.itemCount}}</td></tr>\r\n    <tr>\r\n      <th>Prix total</th>\r\n      <td>{{order.cart.totalPrice | currency:EUR:true }}</td>\r\n    </tr>\r\n  </table>\r\n  <div class=\"text-center pt-2\">\r\n    <button class=\"btn btn-outline-primary\" routerLink=\"/checkout/step2\">\r\n      Back\r\n    </button>\r\n    <button class=\"btn btn-danger\" (click)=\"submitOrder()\">\r\n      Commander\r\n    </button>\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ 150:
/***/ (function(module, exports) {

module.exports = "<h2 class=\"text-center\">Order Confirmation</h2>\r\n<div class=\"container\">\r\n  <table *ngIf=\"order.orderConfirmation; else nodata\" class=\"table m-2\">\r\n    <tr><th>Commande</th><td>{{order.orderConfirmation.orderId}}</td></tr>\r\n    <tr><th>Prix</th><td>{{order.orderConfirmation.amount}}</td></tr>\r\n    <tr><th>Code de payement</th><td>{{order.orderConfirmation.authCode}}</td></tr>\r\n  </table>\r\n  <div class=\"text-center\">\r\n    <button class=\"btn btn-primary\" routerLink=\"/\">Finish</button>\r\n  </div>\r\n  <ng-template #nodata>\r\n    <h3 class=\"text-center\">Envoie de la commande...</h3>\r\n  </ng-template>\r\n</div>\r\n"

/***/ }),

/***/ 151:
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"pages.length > 1\" class=\"text-right my-2\">\r\n  <button *ngFor=\"let page of pages\"\r\n          class=\"btn btn-outline-primary mx-1\"\r\n          [class.btn-primary]=\"current == page\"\r\n          (click)=\"changePage(page)\">\r\n    {{page}}\r\n  </button>\r\n</div>\r\n"

/***/ }),

/***/ 152:
/***/ (function(module, exports) {

module.exports = "<div *ngIf=\"products?.length > 0; else nodata\" class=\"row\">\r\n  <div class=\"col-md-12\">\r\n    <div class=\"portlet light portlet-fit \">\r\n      <div class=\"portlet-title\">\r\n        <div class=\"caption\">\r\n          <i class=\" icon-layers font-green\"></i>\r\n          <span class=\"caption-subject font-green bold uppercase\">Liste des produits</span>\r\n        </div>\r\n      </div>\r\n      <div class=\"portlet-body\">\r\n        <div class=\"mt-element-card mt-element-overlay\">\r\n          <div class=\"row\">\r\n            <div *ngFor=\"let product of products\" class=\"col-lg-4 col-md-4 col-sm-6 col-xs-12\">\r\n              <div class=\"mt-card-item\">\r\n                <div class=\"mt-card-avatar mt-overlay-1\">\r\n                  <img src=\"../assets/pages/img/avatars/team1.jpg\" />\r\n                  <div class=\"mt-overlay\">\r\n                    <ul class=\"mt-info\">\r\n                      <li>\r\n                        <a class=\"btn default btn-outline\" href=\"javascript:;\">\r\n                          <i class=\"icon-magnifier\"></i>\r\n                        </a>\r\n                      </li>\r\n                      <li>\r\n                        <a class=\"btn default btn-outline\" href=\"javascript:;\">\r\n                          <i class=\"icon-link\"></i>\r\n                        </a>\r\n                      </li>\r\n                    </ul>\r\n                  </div>\r\n                </div>\r\n                <div class=\"mt-card-content\">\r\n                  <h3 class=\"mt-card-name\">{{product.name}}</h3>\r\n                  <h4>{{product.price | currency:\"EUR\":true}}</h4>\r\n                  <p class=\"mt-card-desc font-grey-mint\">{{product.description}}</p>\r\n                  <div class=\"mt-card-social\">\r\n                    <store-ratings [product]=\"product\"></store-ratings>\r\n                    <button class=\"float-right btn btn-sm btn-success\"\r\n                            (click)=\"addToCart(product)\">\r\n                      Add to Cart\r\n                    </button>\r\n                  </div>\r\n                </div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        </div>\r\n      </div>\r\n    </div>\r\n  </div>\r\n</div>\r\n\r\n  <ng-template #nodata>\r\n    <h4 class=\"m-1\">Waiting for data...</h4>\r\n  </ng-template>\r\n"

/***/ }),

/***/ 153:
/***/ (function(module, exports) {

module.exports = "<div class=\"wrapper\">\r\n  <!-- BEGIN HEADER -->\r\n  <header class=\"page-header\">\r\n    <nav class=\"navbar mega-menu\" role=\"navigation\">\r\n      <div class=\"container-fluid\">\r\n        <div class=\"clearfix navbar-fixed-top\">\r\n          <!-- Brand and toggle get grouped for better mobile display -->\r\n          <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-responsive-collapse\">\r\n            <span class=\"sr-only\">Toggle navigation</span>\r\n            <span class=\"toggle-icon\">\r\n              <span class=\"icon-bar\"></span>\r\n              <span class=\"icon-bar\"></span>\r\n              <span class=\"icon-bar\"></span>\r\n            </span>\r\n          </button>\r\n          <!-- End Toggle Button -->\r\n          <!-- BEGIN LOGO -->\r\n          <a id=\"index\" class=\"page-logo\" routerLink=\"/\">\r\n            RovaShop\r\n          </a>\r\n          <!-- END LOGO -->\r\n          <!-- BEGIN SEARCH -->\r\n          <div class=\"search\">\r\n            <input type=\"name\" class=\"form-control\" name=\"query\" placeholder=\"Search...\">\r\n            <a href=\"javascript:;\" class=\"btn submit md-skip\">\r\n              <i class=\"fa fa-search\"></i>\r\n            </a>\r\n          </div>\r\n          <div class=\"topbar-actions\">\r\n            <store-cartsummary></store-cartsummary>\r\n          </div>\r\n        </div>\r\n        <!-- BEGIN HEADER MENU -->\r\n        <div class=\"nav-collapse collapse navbar-collapse navbar-responsive-collapse\">\r\n          <ul class=\"nav navbar-nav\">\r\n            <li class=\"dropdown dropdown-fw dropdown-fw-disabled  active open selected\" routerLink=\"/\" routerLinkActive=\"active open selected\">\r\n              <a href=\"javascript:;\" class=\"text-uppercase\">\r\n                <i class=\"icon-home\"></i> Acceuil\r\n              </a>\r\n            </li>\r\n            <li class=\"dropdown dropdown-fw dropdown-fw-disabled\" routerLink=\"/contact\" routerLinkActive=\"active\">\r\n              <a href=\"javascript:;\" class=\"text-uppercase\">\r\n                <i class=\"icon-puzzle\"></i> Contact\r\n              </a>\r\n            </li>\r\n          </ul>\r\n        </div>\r\n        <!-- END HEADER MENU -->\r\n      </div>\r\n      <!--/container-->\r\n    </nav>\r\n  </header>\r\n  <!-- END HEADER -->\r\n  <div class=\"container-fluid\">\r\n    <div class=\"page-content\">\r\n      <div class=\"page-container\">\r\n        <div class=\"row\">\r\n          <div class=\"col-lg-3\">\r\n            <store-categoryfilter></store-categoryfilter>\r\n          </div>\r\n          <!-- /.col-lg-3 -->\r\n          <div class=\"col-lg-9\">\r\n            <div class=\"row\">\r\n              <store-product-list></store-product-list>\r\n\r\n            </div>\r\n            <div class=\"row\">\r\n              <store-pagination></store-pagination>\r\n            </div>\r\n            <!-- /.row -->\r\n          </div>\r\n          <!-- /.col-lg-9 -->\r\n        </div>\r\n      </div>\r\n\r\n    </div>\r\n</div>\r\n</div>\r\n"

/***/ }),

/***/ 154:
/***/ (function(module, exports) {

module.exports = "<span class=\"h6 ml-1\">\r\n  <i *ngFor=\"let s of stars\"\r\n     [class]=\"s ? 'fa fa-star' : 'fa fa-star-o'\"\r\n     [style.color]=\"s ? 'goldenrod' : 'gray'\">\r\n  </i>\r\n</span>\r\n"

/***/ }),

/***/ 23:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__repository__ = __webpack_require__(6);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Cart; });
/* unused harmony export ProductSelection */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var Cart = (function () {
    function Cart(repo) {
        var _this = this;
        this.repo = repo;
        this.selections = [];
        this.itemCount = 0;
        this.totalPrice = 0;
        repo.getSessionData("cart").subscribe(function (cartData) {
            if (cartData != null) {
                cartData.map(function (item) { return new ProductSelection(_this, item.productId, item.name, item.price, item.quantity); })
                    .forEach(function (item) { return _this.selections.push(item); });
                _this.update(false);
            }
        });
    }
    Cart.prototype.addProduct = function (product) {
        var selection = this.selections
            .find(function (ps) { return ps.productId == product.productId; });
        if (selection) {
            selection.quantity++;
        }
        else {
            this.selections.push(new ProductSelection(this, product.productId, product.name, product.price, 1));
        }
        this.update();
    };
    Cart.prototype.updateQuantity = function (productId, quantity) {
        if (quantity > 0) {
            var selection = this.selections.find(function (ps) { return ps.productId == productId; });
            if (selection) {
                selection.quantity = quantity;
            }
        }
        else {
            var index = this.selections.findIndex(function (ps) { return ps.productId == productId; });
            if (index != -1) {
                this.selections.splice(index, 1);
            }
            this.update();
        }
    };
    Cart.prototype.clear = function () {
        this.selections = [];
        this.update();
    };
    Cart.prototype.update = function (storeData) {
        if (storeData === void 0) { storeData = true; }
        this.itemCount = this.selections.map(function (ps) { return ps.quantity; })
            .reduce(function (prev, curr) { return prev + curr; }, 0);
        this.totalPrice = this.selections.map(function (ps) { return ps.price * ps.quantity; })
            .reduce(function (prev, curr) { return prev + curr; }, 0);
        if (storeData) {
            this.repo.storeSessionData("cart", this.selections.map(function (s) {
                return {
                    productId: s.productId, name: s.name,
                    price: s.price, quantity: s.quantity
                };
            }));
        }
    };
    return Cart;
}());
Cart = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* Injectable */])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__repository__["a" /* Repository */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__repository__["a" /* Repository */]) === "function" && _a || Object])
], Cart);

var ProductSelection = (function () {
    function ProductSelection(cart, productId, name, price, quantityValue) {
        this.cart = cart;
        this.productId = productId;
        this.name = name;
        this.price = price;
        this.quantityValue = quantityValue;
    }
    Object.defineProperty(ProductSelection.prototype, "quantity", {
        get: function () {
            return this.quantityValue;
        },
        set: function (newQuantity) {
            this.quantityValue = newQuantity;
            this.cart.update();
        },
        enumerable: true,
        configurable: true
    });
    return ProductSelection;
}());

var _a;
//# sourceMappingURL=cart.model.js.map

/***/ }),

/***/ 24:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__cart_model__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__repository__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_filter__ = __webpack_require__(223);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_filter___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_operator_filter__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Order; });
/* unused harmony export Payment */
/* unused harmony export CartLine */
/* unused harmony export OrderConfirmation */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var Order = (function () {
    function Order(repo, cart, router) {
        var _this = this;
        this.repo = repo;
        this.cart = cart;
        this.payment = new Payment();
        this.submitted = false;
        this.shipped = false;
        router.events
            .filter(function (event) { return event instanceof __WEBPACK_IMPORTED_MODULE_3__angular_router__["c" /* NavigationStart */]; })
            .subscribe(function (event) {
            if (router.url.startsWith("/checkout")
                && _this.name != null && _this.address != null) {
                repo.storeSessionData("checkout", {
                    name: _this.name,
                    address: _this.address,
                    cardNumber: _this.payment.cardNumber,
                    cardExpiry: _this.payment.cardExpiry,
                    cardSecurityCode: _this.payment.cardSecurityCode
                });
            }
        });
        repo.getSessionData("checkout").subscribe(function (data) {
            if (data != null) {
                _this.name = data.name;
                _this.address = data.address;
                _this.payment.cardNumber = data.cardNumber;
                _this.payment.cardExpiry = data.cardExpiry;
                _this.payment.cardSecurityCode = data.cardSecurityCode;
            }
        });
    }
    Object.defineProperty(Order.prototype, "products", {
        get: function () {
            return this.cart.selections
                .map(function (p) { return new CartLine(p.productId, p.quantity); });
        },
        enumerable: true,
        configurable: true
    });
    Order.prototype.clear = function () {
        this.name = null;
        this.address = null;
        this.payment = new Payment();
        this.cart.clear();
        this.submitted = false;
    };
    Order.prototype.submit = function () {
        this.submitted = true;
        this.repo.createOrder(this);
    };
    return Order;
}());
Order = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* Injectable */])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__repository__["a" /* Repository */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__repository__["a" /* Repository */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__cart_model__["a" /* Cart */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__cart_model__["a" /* Cart */]) === "function" && _b || Object, typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* Router */]) === "function" && _c || Object])
], Order);

var Payment = (function () {
    function Payment() {
    }
    return Payment;
}());

var CartLine = (function () {
    function CartLine(productId, quantity) {
        this.productId = productId;
        this.quantity = quantity;
    }
    return CartLine;
}());

var OrderConfirmation = (function () {
    function OrderConfirmation(orderId, authCode, amount) {
        this.orderId = orderId;
        this.authCode = authCode;
        this.amount = amount;
    }
    return OrderConfirmation;
}());

var _a, _b, _c;
//# sourceMappingURL=order.model.js.map

/***/ }),

/***/ 269:
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(22);
module.exports = __webpack_require__(118);


/***/ }),

/***/ 30:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_repository__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_observable_of__ = __webpack_require__(221);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_add_observable_of___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_add_observable_of__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthenticationService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AuthenticationService = (function () {
    function AuthenticationService(repo, router) {
        this.repo = repo;
        this.router = router;
        this.authenticated = false;
    }
    AuthenticationService.prototype.login = function () {
        var _this = this;
        this.authenticated = false;
        return this.repo.login(this.name, this.password)
            .map(function (response) {
            console.log("response ", response);
            if (response.ok) {
                _this.authenticated = true;
                _this.password = null;
                _this.router.navigateByUrl(_this.callbackUrl || "/admin/overview");
            }
            return _this.authenticated;
        })
            .catch(function (e) {
            _this.authenticated = false;
            return __WEBPACK_IMPORTED_MODULE_2_rxjs_Observable__["Observable"].of(false);
        });
    };
    AuthenticationService.prototype.logout = function () {
        this.authenticated = false;
        this.repo.logout();
        this.router.navigateByUrl("/login");
    };
    return AuthenticationService;
}());
AuthenticationService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* Injectable */])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__models_repository__["a" /* Repository */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__models_repository__["a" /* Repository */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_3__angular_router__["b" /* Router */]) === "function" && _b || Object])
], AuthenticationService);

var _a, _b;
//# sourceMappingURL=authentication.service.js.map

/***/ }),

/***/ 40:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__ = __webpack_require__(21);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ErrorHandlerService; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return ValidationError; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var ErrorHandlerService = (function () {
    function ErrorHandlerService() {
        this.subject = new __WEBPACK_IMPORTED_MODULE_1_rxjs_Subject__["Subject"]();
    }
    ErrorHandlerService.prototype.handleError = function (error) {
        var _this = this;
        setTimeout(function () {
            if (error instanceof ValidationError) {
                _this.subject.next(error.errors);
            }
            else if (error instanceof Error) {
                _this.subject.next([error.message]);
            }
            else {
                _this.subject.next(["An error has occurred"]);
            }
        });
    };
    Object.defineProperty(ErrorHandlerService.prototype, "errors", {
        get: function () {
            return this.subject;
        },
        enumerable: true,
        configurable: true
    });
    return ErrorHandlerService;
}());
ErrorHandlerService = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* Injectable */])()
], ErrorHandlerService);

var ValidationError = (function () {
    function ValidationError(errors) {
        this.errors = errors;
    }
    return ValidationError;
}());

//# sourceMappingURL=errorHandler.service.js.map

/***/ }),

/***/ 6:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(87);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(224);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__configClasses_repository__ = __webpack_require__(127);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__errorHandler_service__ = __webpack_require__(40);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_catch__ = __webpack_require__(222);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_catch___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_rxjs_add_operator_catch__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Repository; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var productsUrl = "/api/products";
var suppliersUrl = "/api/suppliers";
var ordersUrl = "/api/order";
var Repository = (function () {
    function Repository(http) {
        this.http = http;
        this.filterObject = new __WEBPACK_IMPORTED_MODULE_3__configClasses_repository__["a" /* Filter */]();
        this.paginationObject = new __WEBPACK_IMPORTED_MODULE_3__configClasses_repository__["b" /* Pagination */]();
        this.orders = [];
        this.suppliers = [];
        this.categories = [];
        this.filter.related = true;
        this.getProducts();
    }
    Repository.prototype.getProduct = function (id) {
        var _this = this;
        console.log("url ", productsUrl + "/" + id);
        this.sendRequest(__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* RequestMethod */].Get, productsUrl + "/" + id).subscribe(function (response) { _this.product = response; });
    };
    Repository.prototype.sendRequest = function (verb, url, data) {
        return this.http.request(new __WEBPACK_IMPORTED_MODULE_1__angular_http__["c" /* Request */]({
            method: verb,
            url: url,
            body: data
        })).map(function (response) {
            return response.headers.get("Content-Length") != "0"
                ? response.json() : null;
        })
            .catch(function (errorResponse) {
            if (errorResponse.status == 400) {
                var jsonData_1;
                try {
                    jsonData_1 = errorResponse.json();
                }
                catch (e) {
                    throw new Error("Network Error");
                }
                var messages = Object.getOwnPropertyNames(jsonData_1)
                    .map(function (p) { return jsonData_1[p]; });
                throw new __WEBPACK_IMPORTED_MODULE_4__errorHandler_service__["b" /* ValidationError */](messages);
            }
            console.log("url : ", url, "error ", errorResponse);
            throw new Error("Network Error");
        });
        ;
    };
    Repository.prototype.getProducts = function (related) {
        var _this = this;
        if (related === void 0) { related = false; }
        var url = productsUrl + "?related=" + this.filter.related;
        if (this.filter.category) {
            url += "&category=" + this.filter.category;
        }
        if (this.filter.search) {
            url += "&search=" + this.filter.search;
        }
        url += "&metadata=true";
        this.sendRequest(__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* RequestMethod */].Get, url)
            .subscribe(function (response) {
            _this.products = response.data;
            _this.categories = response.categories;
            _this.pagination.currentPage = 1;
        });
    };
    Repository.prototype.getSuppliers = function () {
        var _this = this;
        this.sendRequest(__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* RequestMethod */].Get, suppliersUrl)
            .subscribe(function (response) {
            console.log("response ", response);
            _this.suppliers = response;
        });
    };
    Repository.prototype.createProduct = function (prod) {
        var _this = this;
        var data = {
            name: prod.name, category: prod.category,
            description: prod.description, price: prod.price,
            supplier: prod.supplier ? prod.supplier.supplierId : 0
        };
        this.sendRequest(__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* RequestMethod */].Post, productsUrl, data)
            .subscribe(function (response) {
            prod.productId = response;
            _this.products.push(prod);
        });
    };
    Repository.prototype.createProductAndSupplier = function (prod, supp) {
        var _this = this;
        var data = {
            name: supp.name, city: supp.city, state: supp.state
        };
        this.sendRequest(__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* RequestMethod */].Post, suppliersUrl, data)
            .subscribe(function (response) {
            supp.supplierId = response;
            prod.supplier = supp;
            _this.suppliers.push(supp);
            if (prod != null) {
                _this.createProduct(prod);
            }
        });
    };
    Repository.prototype.replaceProduct = function (prod) {
        var _this = this;
        var data = {
            name: prod.name, category: prod.category,
            description: prod.description, price: prod.price,
            supplier: prod.supplier ? prod.supplier.supplierId : 0
        };
        this.sendRequest(__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* RequestMethod */].Put, productsUrl + "/" + prod.productId, data)
            .subscribe(function (response) { return _this.getProducts(); });
    };
    Repository.prototype.replaceSupplier = function (supp) {
        var _this = this;
        var data = {
            name: supp.name, city: supp.city, state: supp.state
        };
        this.sendRequest(__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* RequestMethod */].Put, suppliersUrl + "/" + supp.supplierId, data)
            .subscribe(function (response) { return _this.getProducts(); });
    };
    Repository.prototype.updateProduct = function (id, changes) {
        var _this = this;
        var patch = [];
        changes.forEach(function (value, key) {
            return patch.push({ op: "replace", path: key, value: value });
        });
        this.sendRequest(__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* RequestMethod */].Patch, productsUrl + "/" + id, patch)
            .subscribe(function (response) { return _this.getProducts(); });
    };
    Repository.prototype.deleteProduct = function (id) {
        var _this = this;
        this.sendRequest(__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* RequestMethod */].Delete, productsUrl + "/" + id)
            .subscribe(function (response) { return _this.getProducts(); });
    };
    Repository.prototype.deleteSupplier = function (id) {
        var _this = this;
        this.sendRequest(__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* RequestMethod */].Delete, suppliersUrl + "/" + id)
            .subscribe(function (response) {
            _this.getProducts();
            _this.getSuppliers();
        });
    };
    //session
    Repository.prototype.storeSessionData = function (dataType, data) {
        return this.sendRequest(__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* RequestMethod */].Post, "/api/session/" + dataType, data)
            .subscribe(function (response) { });
    };
    Repository.prototype.getSessionData = function (dataType) {
        return this.sendRequest(__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* RequestMethod */].Get, "/api/session/" + dataType);
    };
    //orders
    Repository.prototype.getOrders = function () {
        var _this = this;
        this.sendRequest(__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* RequestMethod */].Get, ordersUrl)
            .subscribe(function (data) { return _this.orders = data; });
    };
    Repository.prototype.createOrder = function (order) {
        this.sendRequest(__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* RequestMethod */].Post, ordersUrl, {
            name: order.name,
            address: order.address,
            payment: order.payment,
            products: order.products
        }).subscribe(function (data) {
            order.orderConfirmation = data;
            order.cart.clear();
            order.clear();
        });
    };
    Repository.prototype.shipOrder = function (order) {
        var _this = this;
        this.sendRequest(__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* RequestMethod */].Post, ordersUrl + "/" + order.orderId)
            .subscribe(function (r) { return _this.getOrders(); });
    };
    //login
    Repository.prototype.login = function (name, password) {
        return this.http.post("/api/account/login", { name: name, password: password });
    };
    Repository.prototype.logout = function () {
        this.sendRequest(__WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* RequestMethod */].Post, "/api/account/logout", null).subscribe(function (respone) { });
    };
    Object.defineProperty(Repository.prototype, "filter", {
        get: function () {
            return this.filterObject;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(Repository.prototype, "pagination", {
        get: function () {
            return this.paginationObject;
        },
        enumerable: true,
        configurable: true
    });
    return Repository;
}());
Repository = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* Injectable */])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Http */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* Http */]) === "function" && _a || Object])
], Repository);

var _a;
//# sourceMappingURL=repository.js.map

/***/ }),

/***/ 73:
/***/ (function(module, exports) {

function webpackEmptyContext(req) {
	throw new Error("Cannot find module '" + req + "'.");
}
webpackEmptyContext.keys = function() { return []; };
webpackEmptyContext.resolve = webpackEmptyContext;
module.exports = webpackEmptyContext;
webpackEmptyContext.id = 73;

/***/ }),

/***/ 74:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_repository__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__auth_authentication_service__ = __webpack_require__(30);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdminComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AdminComponent = (function () {
    function AdminComponent(repo, authService) {
        this.repo = repo;
        this.authService = authService;
        repo.filter.reset();
        repo.filter.related = true;
        this.repo.getProducts();
        this.repo.getSuppliers();
        this.repo.getOrders();
    }
    return AdminComponent;
}());
AdminComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* Component */])({
        template: __webpack_require__(137),
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__models_repository__["a" /* Repository */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__models_repository__["a" /* Repository */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__auth_authentication_service__["a" /* AuthenticationService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__auth_authentication_service__["a" /* AuthenticationService */]) === "function" && _b || Object])
], AdminComponent);

var _a, _b;
//# sourceMappingURL=admin.component.js.map

/***/ }),

/***/ 75:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_repository__ = __webpack_require__(6);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OrderAdminComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var OrderAdminComponent = (function () {
    function OrderAdminComponent(repo) {
        this.repo = repo;
    }
    Object.defineProperty(OrderAdminComponent.prototype, "orders", {
        get: function () {
            return this.repo.orders;
        },
        enumerable: true,
        configurable: true
    });
    OrderAdminComponent.prototype.markShipped = function (order) {
        this.repo.shipOrder(order);
    };
    return OrderAdminComponent;
}());
OrderAdminComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* Component */])({
        template: __webpack_require__(138)
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__models_repository__["a" /* Repository */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__models_repository__["a" /* Repository */]) === "function" && _a || Object])
], OrderAdminComponent);

var _a;
//# sourceMappingURL=orderAdmin.component.js.map

/***/ }),

/***/ 76:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_repository__ = __webpack_require__(6);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OverviewComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var OverviewComponent = (function () {
    function OverviewComponent(repo) {
        this.repo = repo;
    }
    Object.defineProperty(OverviewComponent.prototype, "products", {
        get: function () {
            return this.repo.products;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(OverviewComponent.prototype, "orders", {
        get: function () {
            return this.repo.orders;
        },
        enumerable: true,
        configurable: true
    });
    return OverviewComponent;
}());
OverviewComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* Component */])({
        template: __webpack_require__(139)
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__models_repository__["a" /* Repository */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__models_repository__["a" /* Repository */]) === "function" && _a || Object])
], OverviewComponent);

var _a;
//# sourceMappingURL=overview.component.js.map

/***/ }),

/***/ 77:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_repository__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__models_product_model__ = __webpack_require__(80);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductAdminComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ProductAdminComponent = (function () {
    function ProductAdminComponent(repo) {
        this.repo = repo;
        this.tableMode = true;
    }
    Object.defineProperty(ProductAdminComponent.prototype, "product", {
        get: function () {
            return this.repo.product;
        },
        enumerable: true,
        configurable: true
    });
    ProductAdminComponent.prototype.selectProduct = function (id) {
        this.repo.getProduct(id);
    };
    ProductAdminComponent.prototype.saveProduct = function () {
        if (this.repo.product.productId == null) {
            this.repo.createProduct(this.repo.product);
        }
        else {
            this.repo.replaceProduct(this.repo.product);
        }
        this.clearProduct();
        this.tableMode = true;
    };
    ProductAdminComponent.prototype.deleteProduct = function (id) {
        this.repo.deleteProduct(id);
    };
    ProductAdminComponent.prototype.clearProduct = function () {
        this.repo.product = new __WEBPACK_IMPORTED_MODULE_2__models_product_model__["a" /* Product */]();
        this.tableMode = true;
    };
    Object.defineProperty(ProductAdminComponent.prototype, "products", {
        get: function () {
            return this.repo.products;
        },
        enumerable: true,
        configurable: true
    });
    return ProductAdminComponent;
}());
ProductAdminComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* Component */])({
        template: __webpack_require__(140)
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__models_repository__["a" /* Repository */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__models_repository__["a" /* Repository */]) === "function" && _a || Object])
], ProductAdminComponent);

var _a;
//# sourceMappingURL=productAdmin.component.js.map

/***/ }),

/***/ 78:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__authentication_service__ = __webpack_require__(30);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthenticationComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AuthenticationComponent = (function () {
    function AuthenticationComponent(authService) {
        this.authService = authService;
        this.showError = false;
    }
    AuthenticationComponent.prototype.login = function () {
        var _this = this;
        this.showError = false;
        this.authService.login().subscribe(function (result) {
            _this.showError = !result;
        });
    };
    return AuthenticationComponent;
}());
AuthenticationComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* Component */])({
        template: __webpack_require__(143)
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__authentication_service__["a" /* AuthenticationService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__authentication_service__["a" /* AuthenticationService */]) === "function" && _a || Object])
], AuthenticationComponent);

var _a;
//# sourceMappingURL=authentication.component.js.map

/***/ }),

/***/ 79:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__authentication_service__ = __webpack_require__(30);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthenticationGuard; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var AuthenticationGuard = (function () {
    function AuthenticationGuard(router, authService) {
        this.router = router;
        this.authService = authService;
    }
    AuthenticationGuard.prototype.canActivateChild = function (route, state) {
        console.log("this.authService.authenticated ", this.authService.authenticated);
        if (this.authService.authenticated) {
            return true;
        }
        else {
            this.authService.callbackUrl = "/admin/" + route.url.toString();
            this.router.navigateByUrl("/login");
            return false;
        }
    };
    return AuthenticationGuard;
}());
AuthenticationGuard = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["d" /* Injectable */])(),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__authentication_service__["a" /* AuthenticationService */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__authentication_service__["a" /* AuthenticationService */]) === "function" && _b || Object])
], AuthenticationGuard);

var _a, _b;
//# sourceMappingURL=authentication.guard.js.map

/***/ }),

/***/ 80:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Product; });
var Product = (function () {
    function Product(productId, name, category, description, price, supplier, rating) {
        this.productId = productId;
        this.name = name;
        this.category = category;
        this.description = description;
        this.price = price;
        this.supplier = supplier;
        this.rating = rating;
    }
    return Product;
}());

//# sourceMappingURL=product.model.js.map

/***/ }),

/***/ 81:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_cart_model__ = __webpack_require__(23);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CartDetailComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var CartDetailComponent = (function () {
    function CartDetailComponent(cart) {
        this.cart = cart;
    }
    return CartDetailComponent;
}());
CartDetailComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* Component */])({
        template: __webpack_require__(144)
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__models_cart_model__["a" /* Cart */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__models_cart_model__["a" /* Cart */]) === "function" && _a || Object])
], CartDetailComponent);

var _a;
//# sourceMappingURL=cartDetail.component.js.map

/***/ }),

/***/ 82:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__models_order_model__ = __webpack_require__(24);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CheckoutDetailsComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var CheckoutDetailsComponent = (function () {
    function CheckoutDetailsComponent(router, order) {
        this.router = router;
        this.order = order;
        if (order.products.length == 0) {
            this.router.navigateByUrl("/cart");
        }
    }
    return CheckoutDetailsComponent;
}());
CheckoutDetailsComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* Component */])({
        template: __webpack_require__(147)
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__models_order_model__["a" /* Order */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__models_order_model__["a" /* Order */]) === "function" && _b || Object])
], CheckoutDetailsComponent);

var _a, _b;
//# sourceMappingURL=checkoutDetail.component.js.map

/***/ }),

/***/ 83:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__models_order_model__ = __webpack_require__(24);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CheckoutPaymentComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var CheckoutPaymentComponent = (function () {
    function CheckoutPaymentComponent(router, order) {
        this.router = router;
        this.order = order;
        if (order.name == null || order.address == null) {
            router.navigateByUrl("/checkout/step1");
        }
    }
    return CheckoutPaymentComponent;
}());
CheckoutPaymentComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* Component */])({
        template: __webpack_require__(148)
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__models_order_model__["a" /* Order */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__models_order_model__["a" /* Order */]) === "function" && _b || Object])
], CheckoutPaymentComponent);

var _a, _b;
//# sourceMappingURL=checkoutPayement.component.js.map

/***/ }),

/***/ 84:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__models_order_model__ = __webpack_require__(24);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CheckoutSummaryComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var CheckoutSummaryComponent = (function () {
    function CheckoutSummaryComponent(router, order) {
        this.router = router;
        this.order = order;
        if (order.payment.cardNumber == null
            || order.payment.cardExpiry == null
            || order.payment.cardSecurityCode == null) {
            router.navigateByUrl("/checkout/step2");
        }
    }
    CheckoutSummaryComponent.prototype.submitOrder = function () {
        this.order.submit();
        this.router.navigateByUrl("/checkout/confirmation");
    };
    return CheckoutSummaryComponent;
}());
CheckoutSummaryComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* Component */])({
        template: __webpack_require__(149)
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__models_order_model__["a" /* Order */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__models_order_model__["a" /* Order */]) === "function" && _b || Object])
], CheckoutSummaryComponent);

var _a, _b;
//# sourceMappingURL=checkoutSummary.component.js.map

/***/ }),

/***/ 85:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__models_order_model__ = __webpack_require__(24);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OrderConfirmationComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var OrderConfirmationComponent = (function () {
    function OrderConfirmationComponent(router, order) {
        this.router = router;
        this.order = order;
        if (!order.submitted) {
            router.navigateByUrl("/checkout/step3");
        }
    }
    return OrderConfirmationComponent;
}());
OrderConfirmationComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* Component */])({
        template: __webpack_require__(150)
    }),
    __metadata("design:paramtypes", [typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* Router */]) === "function" && _a || Object, typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__models_order_model__["a" /* Order */] !== "undefined" && __WEBPACK_IMPORTED_MODULE_2__models_order_model__["a" /* Order */]) === "function" && _b || Object])
], OrderConfirmationComponent);

var _a, _b;
//# sourceMappingURL=orderConfirmation.component.js.map

/***/ }),

/***/ 86:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProductSelectionComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var ProductSelectionComponent = (function () {
    function ProductSelectionComponent() {
    }
    return ProductSelectionComponent;
}());
ProductSelectionComponent = __decorate([
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_11" /* Component */])({
        selector: "store-products",
        template: __webpack_require__(153)
    })
], ProductSelectionComponent);

//# sourceMappingURL=productSelection.component.js.map

/***/ })

},[269]);
//# sourceMappingURL=main.bundle.js.map