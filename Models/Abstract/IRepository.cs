using rovashop.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rovashop.Models.Abstract
{
  public interface IRepository
  {
    IEnumerable<Product> Products { get; }
    IEnumerable<Supplier> Suppliers { get; }
    IEnumerable<Rating> Ratings { get; }
    void InitSeeds();
    void InitSeedIdentity();
  }
}
