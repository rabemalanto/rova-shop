using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace rovashop.Models.Entity
{
  [Table("supplier")]
  public class Supplier
  {
    [Key]
    public long SupplierId { get; set; }
    public string Name { get; set; }
    public string City { get; set; }
    public string State { get; set; }
    public IEnumerable<Product> Products { get; set; }
  }
}
