using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace rovashop.Models.Entity
{
  [Table("rating")]
  public class Rating
  {
    [Key]
    public long RatingId { get; set; }
    public int Stars { get; set; }
    public Product Product { get; set; }
  }
}
