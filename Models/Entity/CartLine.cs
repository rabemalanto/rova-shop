using Microsoft.AspNetCore.Mvc.ModelBinding;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace rovashop.Models.Entity
{
  public class CartLine
  {
    [BindNever]
    public long CartLineId { get; set; }
    [Required]
    public long ProductId { get; set; }
    [Required]
    public int Quantity { get; set; }
  }
}
