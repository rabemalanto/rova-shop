using rovashop.Models.Abstract;
using rovashop.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rovashop.Models.Concrete
{
  public class EFRepository : IRepository
  {
    private RovaShopDataContext context;
    public EFRepository(RovaShopDataContext ctx)
    {
      context = ctx;
    }
    public IEnumerable<Product> Products { get => context.Products; }
    public IEnumerable<Supplier> Suppliers { get => context.Suppliers; }
    public IEnumerable<Rating> Ratings { get => context.Ratings; }
    public void InitSeeds()
    {
      SeedData.SeedDatabase(context);
    }
    public void InitSeedIdentity()
    {
      
    }
  }
}
