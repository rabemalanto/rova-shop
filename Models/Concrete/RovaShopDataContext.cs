using Microsoft.EntityFrameworkCore;
using rovashop.Models.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore.Metadata;

namespace rovashop.Models.Concrete
{
  public class RovaShopDataContext: DbContext
  {
    public RovaShopDataContext(DbContextOptions<RovaShopDataContext> opts): base(opts) { }
    public DbSet<Product> Products { get; set; }
    public DbSet<Supplier> Suppliers { get; set; }
    public DbSet<Rating> Ratings { get; set; }
    public DbSet<Order> Orders { get; set; }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
      modelBuilder.Entity<Product>().HasMany<Rating>(p => p.Ratings)
      .WithOne(r => r.Product).OnDelete(DeleteBehavior.Cascade);
      modelBuilder.Entity<Product>().HasOne<Supplier>(p => p.Supplier)
      .WithMany(s => s.Products).OnDelete(DeleteBehavior.SetNull);
    }
  }
}
