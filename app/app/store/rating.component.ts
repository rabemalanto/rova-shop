import { Product } from "../models/product.model";
import { Component, Input } from "@angular/core";
@Component({
  selector: "store-ratings",
  templateUrl: "ratings.component.html"
})
export class RatingsComponent {
  @Input()
  product: Product;

  get stars(): boolean[] {
    if (this.product != null && this.product.rating != null) {
      let total = this.product.rating.map(r => r.stars)
        .reduce((prev, curr) => prev + curr, 0);
      let count = Math.round(total / this.product.rating.length);
      return Array(5).fill(false).map((value, index) => {
        return index < count;
      });
    } else {
      return [];
    }
  }
}
