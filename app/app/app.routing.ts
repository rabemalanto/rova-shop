import { Routes, RouterModule } from "@angular/router";
//import { ProductTableComponent } from "./structure/productTable.component"
//import { ProductDetailComponent } from "./structure/productDetail.component";
import { ProductSelectionComponent } from "./store/productSelection.component";
import { CartDetailComponent } from "./store/cartDetail.component";
import { CheckoutDetailsComponent }  from "./store/checkout/checkoutDetail.component";
import { CheckoutPaymentComponent }  from "./store/checkout/checkoutPayement.component";
import { CheckoutSummaryComponent }  from "./store/checkout/checkoutSummary.component";
import { OrderConfirmationComponent } from "./store/checkout/orderConfirmation.component";
//admin
import { AdminComponent } from "./admin/admin.component";
import { OverviewComponent } from "./admin/overview.component";
import { ProductAdminComponent } from "./admin/productAdmin.component";
import { OrderAdminComponent } from "./admin/orderAdmin.component";
//login
import { AuthenticationGuard } from "./auth/authentication.guard";
import { AuthenticationComponent } from "./auth/authentication.component";

const routes: Routes = [
  { path: "login", component: AuthenticationComponent },
  { path: "admin", redirectTo: "/admin/overview", pathMatch: "full" },
  //{ path: "table", component: ProductTableComponent },
  //{ path: "detail/:id", component: ProductDetailComponent },
  //{ path: "detail", component: ProductDetailComponent },
  //{ path: "", component: ProductTableComponent }]
  {
    path: "admin", component: AdminComponent,
    canActivateChild: [AuthenticationGuard],
    children: [
      { path: "products", component: ProductAdminComponent },
      { path: "orders", component: OrderAdminComponent },
      { path: "overview", component: OverviewComponent },
      { path: "", component: OverviewComponent }
    ]
  },
  { path: "checkout/step1", component: CheckoutDetailsComponent },
  { path: "checkout/step2", component: CheckoutPaymentComponent },
  { path: "checkout/step3", component: CheckoutSummaryComponent },
  { path: "checkout/confirmation", component: OrderConfirmationComponent },
  { path: "checkout", component: CheckoutDetailsComponent },
  { path: "cart", component: CartDetailComponent },
  { path: "store", component: ProductSelectionComponent },
  { path: "", component: ProductSelectionComponent }]
export const RoutingConfig = RouterModule.forRoot(routes);
