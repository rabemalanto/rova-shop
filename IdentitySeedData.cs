using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using rovashop.Models.Concrete;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace rovashop
{
  public static class IdentitySeedData
  {
    private const string adminUser = "admin";
    private const string adminPassword = "$rovA1*";
    private const string adminRole = "Administrator";
    public static void SeedUsers(UserManager<IdentityUser> userManager)
    {
      
      
      if (userManager.FindByEmailAsync("roockio@gmail.com").Result == null)
      {
        IdentityUser user = new IdentityUser
        {
          UserName = adminUser,
          Email = "roockio@gmail.com"
        };

        IdentityResult result = userManager.CreateAsync(user, adminPassword).Result;
        

        if (result.Succeeded)
        {
          userManager.AddToRoleAsync(user, adminRole).Wait();
        }
      }
    }
  }

  }
